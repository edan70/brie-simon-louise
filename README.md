# Specialized B-tree and BRIE for Datalog evaluation

Btree is a self balancing search tree that implements SortesSet.
It contains hints for faster insertions and serches in the tree. 

As of Alfa1 realease the Btree supports sequential insertion. The parallel inserion is still in development.


## Build and run tests with 

```
./gradlew build
```

## Genereate Test report
```
./gradlew build jacocoTestReport
```

## View Test report (Works on Linux)
```
./viewTestReport.sh
```
O/w open `build/reports/jacoco/test/html/index.html` in your browser of choice.

## Run examples with 
```
./gradlew run
```

## Structure

The src directory contains the implementation of the Btree aswell as tests and examples for it.


1. src/java/BTree/BTree.java contains the implementation of the Btree
2. src/java/BTree/OptimisticReadWriteLock.java contains the implementation of the optimistic read-write lock.
3. src/java/Btree/Lease.java contains the implementation of the lease in the optimistic read-write lock.
4. src/test/BtreeTest contains all the test files for the Btree.
5. src/Examples contains all the Examples of programs using the Btree to store data.

License
-------
This repository is covered by the license BSD 2-clause, see file LICENSE.
