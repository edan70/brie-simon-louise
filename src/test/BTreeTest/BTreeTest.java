package BTreeTest;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;
import BTree.BTree;
public class BTreeTest {
  private class ComparableComparator<T extends Comparable> implements Comparator<T> {
    @Override
    public int compare(T o1, T o2) {
      return o1.compareTo(o2);
    }

    @Override
    public boolean equals(Object o) {
      return o instanceof ComparableComparator;
    }
  }

  private class FloatArrayComparator implements Comparator<float[]> {
    @Override
    public int compare(float[] o1, float[] o2) {
      if (o1.length != o2.length) {
        return o1.length < o2.length ? -1 : 1;
      }

      for (int i = 0; i < o1.length; ++i) {
        if (o1[i] == o2[i]) {
          continue;
        } else {
          if (o1[i] < o2[i]) {
            return -1;
          }
          return 1;
        }
      }
      return 0;
    }

    @Override
    public boolean equals(Object o) {
      return false;
    }
  }
  // FAILS
  @Test public void simplePut() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    bt.add(77);
    bt.add(3);
    bt.add(1);
    bt.add(2);
    bt.add(-1);
    bt.add(99);
  //  bt.printTree(System.out);
    assertEquals(bt.size(), 6);
    bt.printHintStats();
  }

  // PASSES
  @Test public void findInEmpty() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    assertFalse(bt.contains(1));
  }
  // PASSES
  @Test public void findsLast() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for (int i = 0; i < 101; ++i) {
      bt.add(i);
    }

    assertTrue(bt.last().equals(100));
    bt.printHintStats();
  }
  // PASSES
  @Test public void findsFirst() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for (int i = 0; i < 101; ++i) {
      bt.add(i);
    }
    assertTrue(bt.first().equals(0));
    bt.printHintStats();
  }
  // PASSES
  @Test public void simpleDuplicate() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    bt.add(3);
    bt.add(3);
    assertEquals(bt.size(), 1);
    bt.printHintStats();
  }
  // PASSES
  @Test public void depth2() {
    final int ADD = 16;
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for(int i = 0; i < ADD; ++i) {
      bt.add(i);
    }
    bt.printTree(System.out);
    assertEquals(bt.size(), ADD);
    bt.printHintStats();
  }
  // PASSES
  @Test public void depth2_2() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    bt.add(-1);
    bt.add(66);
    bt.add(-2);
    bt.add(99);
    bt.add(-5);
    bt.add(101);
    bt.add(-12);
    bt.add(28);
    bt.add(-33);
    bt.add(200);
    bt.add(-300);
    bt.add(-500);
    bt.add(700);
    bt.add(-1000);
    bt.add(999);
    bt.printTree(System.out);
    assertEquals(bt.size(), 15);
    bt.printHintStats();
  }
  // PASSES
  @Test public void largeInOrderAdd() {
    final int NUM_TO_BE_ADDED = 100;
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for (int i = 0; i < NUM_TO_BE_ADDED; i++) {
      bt.add(i);
    }
    assertEquals(bt.size(), 100);
    bt.printHintStats();
  }
  // FAILS
  @Test public void simpleContains() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    bt.add(-1);
    bt.add(45);
    bt.add(-4);
    bt.add(5);
    bt.add(7);
    bt.add(-12);
    bt.add(23);

    assertTrue(bt.contains(-1));
    assertTrue(bt.contains(45));
    assertTrue(bt.contains(-4));
    assertTrue(bt.contains(5));
    assertTrue(bt.contains(7));
    assertTrue(bt.contains(-12));
    assertTrue(bt.contains(23));
    assertFalse(bt.contains(-9999));
  }

  // FAILS
  @Test public void duplicateContains() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    bt.add(-2);
    bt.add(-4);
    bt.add(12);
    bt.add(3);
    bt.add(5);
    bt.add(-12);
    bt.add(3);
    bt.add(5);
    assertEquals(bt.size(), 6);
    assertTrue(bt.contains(3));
    assertFalse(bt.contains(-999));
  }

  @Test public void largeContain() {
    final int NUM_TO_BE_ADDED = 100;
    //BTree<Integer> bt = new BTree<>();
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for (int i = 0; i < NUM_TO_BE_ADDED; i++) {
      bt.add(i);
    }
    assertEquals(100, bt.size());

    for (int i = 0; i < NUM_TO_BE_ADDED; ++i) {
      assertTrue(bt.contains(i));
    }
    bt.printHintStats();
  }

  @Test public void emptyTest() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    assertTrue(bt.isEmpty());
    bt.add(-2);
    assertFalse(bt.contains(1));
    assertTrue(bt.contains(-2));
  }

  // CAN FAIL
  @Test public void largeRandomAdd() {
    Random rand = new Random();
    final int NUM_TO_BE_ADDED = 100;
    //BTree<Integer> bt = new BTree<>();
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    bt.add(-2);
    Set<Integer> set = new HashSet<>();

    for (int i = 0; i < NUM_TO_BE_ADDED; ++i) {
      int r = rand.nextInt(7);
      r = rand.nextBoolean() ? -r : r;
      set.add(r);
      bt.add(r);
    }

    assertEquals(set.size(), bt.size());
    for (Integer i : set) {
      assertTrue(bt.contains(i));
    }
    bt.printHintStats();
  }


  @Test public void addFloatVectors() {
    float[] a = {1,2,3};
    float[] b = {8,2,3};
    float[] c = {6,9,1};
    float[] d = {4,2,0};
    float[] e = {1,2,3};

    BTree<float[]> bt = new BTree<>(new FloatArrayComparator());
    bt.add(a);
    bt.add(b);
    bt.add(c);
    bt.add(d);
    bt.add(e);

    assertEquals(4, bt.size());
  }

  @Test public void containsFloatVectors() {
    float[] a = {1,2,3};
    float[] b = {4,2,0};

    BTree<float[]> bt = new BTree<>(new FloatArrayComparator());
    bt.add(a);
    bt.add(b);
    assertEquals(2, bt.size());
    assertTrue(bt.contains(a));
    assertTrue(bt.contains(b));

    float[] c = {1,2,3,4};
    float[] d = {1,2,4};
    float[] e = {4,2,1};
    assertFalse(bt.contains(c));
    assertFalse(bt.contains(d));
    assertFalse(bt.contains(e));
    bt.printHintStats();
  }
  // CAN FAIL
  @Test public void customOrderLargeAdd() {
    Random rand = new Random();
    final int NUM_TO_BE_ADDED = 100;
    final int ORDER           = 52;
    BTree<Integer> bt = new BTree<>(ORDER, new ComparableComparator());
    Set<Integer> set = new HashSet<>();

    for (int i = 0; i < NUM_TO_BE_ADDED; ++i) {
      int r = rand.nextInt(6);
      r = rand.nextBoolean() ? -r : r;
      set.add(r);
      bt.add(r);
    }

    assertEquals(set.size(), bt.size());
    for (Integer i : set) {
      assertTrue(bt.contains(i));
    }
    bt.printHintStats();
  }

  @Test public void IteratorfindsFirstNode() {
    BTree<Integer> bt = new BTree(new ComparableComparator());
    bt.add(1);
    bt.add(2);
    bt.add(3);
    bt.add(4);
    bt.add(5);
    bt.add(6);
    bt.add(7);
    Iterator<Integer> it = bt.iterator();
    bt.printTree(System.out);
    assertEquals(it.toString(), "LeafNode: 1 2 3 ");
    bt.printHintStats();
  }
  @Test public void iteratorMovesToParent() {
    BTree<Integer> bt = new BTree(new ComparableComparator());
    bt.add(0);
    bt.add(1);
    bt.add(2);
    bt.add(3);
    bt.add(4);
    bt.add(5);
    bt.add(6);
    Iterator<Integer> it = bt.iterator();
    bt.printTree(System.out);
    for (int i = 0; i < 3; ++i) {
      Integer value = it.next();
      System.out.println(value);
      assertTrue(value.equals(i));
    }

    assertTrue(it.next().equals(3));
    assertEquals(it.toString(), "InnerNode: 3 ");
    bt.printHintStats();
  }

  @Test public void iteratorSimpleMovesToChild() {
    BTree<Integer> bt = new BTree(new ComparableComparator());
    bt.add(0);
    bt.add(1);
    bt.add(2);
    bt.add(3);
    bt.add(4);
    bt.add(5);
    bt.add(6);
    Iterator<Integer> it = bt.iterator();
    bt.printTree(System.out);
    for (int i = 0; i < 4; ++i)
      it.next();
    assertTrue(it.next().equals(4));
    assertEquals("LeafNode: 4 5 6 ", it.toString());
    assertTrue(it.next().equals(5));
    assertTrue(it.next().equals(6));
    bt.printHintStats();
  }

  @Test public void iteratorOnlyRoot() {
    BTree<Integer> bt = new BTree(new ComparableComparator());
    bt.add(0);
    bt.add(1);
    bt.add(2);
    Iterator it = bt.iterator();
    for (int i = 0; i < 3; ++i)
      assertTrue(it.next().equals(i));
  }
  @Test public void iteratorForEachOnlyRoot() {
    BTree<Integer> bt = new BTree(new ComparableComparator());
    bt.add(0);
    bt.add(1);
    bt.add(2);
    int index = 0;
    for (Integer i: bt) {
      assertTrue(i.equals(index++));
    }

    bt.printHintStats();
  }

  @Test public void iteratorDepth2() {
    final int ADD = 16;
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for(int i = 0; i < ADD; ++i) {
      bt.add(i);
    }

    int index = 0;
    for (Integer i : bt) {
      assertTrue(i.equals(index++));
    }
    assertEquals(bt.size(), ADD);
    bt.printHintStats();
  }
  @Test public void largeIterator() {
    final int NUM_TO_BE_ADDED = 100;
    //BTree<Integer> bt = new BTree<>();
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for (int i = 0; i < NUM_TO_BE_ADDED; i++) {
      bt.add(i);
    }

    bt.printTree(System.out);
    int index = 0;
    for (Integer i : bt) {
      assertTrue(i.equals(index++));
    }
    bt.printHintStats();
  }
  @Test public void subSetTest() {
    BTree<Integer> bt = new BTree<Integer>(new ComparableComparator());
    for (int i = 0; i < 50; ++i) {
      bt.add(i);
    }

    SortedSet<Integer> sub = bt.subSet(0,25);
    SortedSet<Integer> sub1 = bt.subSet(10,20);
    SortedSet<Integer> sub2 = bt.subSet(7, 12);

    for (int i = 0; i < 25; ++i) {
      assertTrue(sub.contains(i));
    }
    assertFalse(sub.contains(26));


    assertFalse(sub1.contains(9));
    for (int i = 10; i < 20; ++i) {
      assertTrue(sub1.contains(i));
    }
    assertFalse(sub1.contains(20));

    assertFalse(sub2.contains(6));
    for (int i = 7; i < 12; ++i) {
      assertTrue(sub2.contains(i));
    }
    assertFalse(sub2.contains(12));
  }
  /* PASSES */
  @Test public void testAddAll() {
    ArrayList<Integer> arr = new ArrayList<>();
    arr.add(1);
    arr.add(2);
    arr.add(3);
    arr.add(4);
    arr.add(5);
    arr.add(6);

    BTree<Integer> bt = new BTree<>(new ComparableComparator());

    bt.addAll(arr);

    int index = 1;
    for (Integer i : bt) {
      assertTrue(i.equals(index++));
    }
  }
  // FAILS
  @Test public void testAddAllAllDuplicates() {
    ArrayList<Integer> arr = new ArrayList<>();
    arr.add(1);
    arr.add(2);
    arr.add(3);
    arr.add(4);
    arr.add(5);
    arr.add(6);

    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    bt.add(1);
    bt.add(2);
    bt.add(3);
    bt.add(4);
    bt.add(5);
    bt.add(6);

    assertFalse(bt.addAll(arr));

    int index = 1;
    for (Integer i : bt) {
      assertTrue(i.equals(index++));
    }
    bt.printHintStats();
  }

  // FAILS
  @Test public void testAddAllSomeDuplicates() {
    ArrayList<Integer> arr = new ArrayList<>();
    arr.add(1);
    arr.add(2);
    arr.add(3);
    arr.add(4);
    arr.add(5);
    arr.add(6);

    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    bt.add(1);
    bt.add(2);
    bt.add(3);
    bt.add(5);
    bt.add(6);

    assertTrue(bt.addAll(arr));

    int index = 1;
    for (Integer i : bt) {
      assertTrue(i.equals(index++));
    }
    bt.printHintStats();
  }

  /* PASSES */
  @Test public void headSetTest(){
      BTree<Integer> bt = new BTree<>(new ComparableComparator());
      for (int i = 0; i < 50; ++i) {
        bt.add(i);
      }
      SortedSet<Integer> sub = bt.headSet(25);
      for (int i = 0; i < 25; ++i) {
        assertTrue(sub.contains(i));
      }
      assertFalse(sub.contains(25));
      bt.printHintStats();
  }
  /* PASSES */
  @Test public void tailSetTest(){
      BTree<Integer> bt = new BTree<>(new ComparableComparator());
      for (int i = 0; i < 50; ++i) {
        bt.add(i);
      }

      SortedSet<Integer> sub = bt.tailSet(25);
      for (int i = 25; i < 50; ++i) {
        assertTrue(sub.contains(i));
      
      }
      bt.printHintStats();
    //  assertFalse(sub.contains(24));
  }

  /* PASSES */
  @Test public void equalsTest() {
    BTree<Integer> bt1 = new BTree<>(new ComparableComparator());
    BTree<Integer> bt2 = new BTree<>(new ComparableComparator());
    BTree<Integer> bt3 = new BTree<>(new ComparableComparator());
    BTree<Integer> bt4 = new BTree<>(new ComparableComparator());

    for (int i = 0; i < 100; ++i){
      bt1.add(i);
      bt2.add(i);
      bt4.add(i);
      if (i > 42) {
        bt3.add(i);
      }
    }
    bt4.add(100);
    assertFalse(bt4.equals(bt1));
    assertTrue(bt1.equals(bt1)); // Equal to itself
    assertTrue(bt1.equals(bt2)); // Equal to identical copy
    assertFalse(bt1.equals(bt3)); // Equal to identical copy
    bt1.add(100);
    assertFalse(bt1.equals(bt2)); // Test different number of elements in tree
    bt2.add(-1);
    assertFalse(bt1.equals(bt2)); // Test Contains different elements
    assertFalse(bt1.equals(new ArrayList<Integer>())); // Test equal to other kind of Object.
  }

  /* PASSES */
  @Test public void removalsUndefined() {
    BTree<Integer> bt1 = new BTree<>(new ComparableComparator());
    BTree<Integer> bt2 = new BTree<>(new ComparableComparator());
    ArrayList<Integer> arr = new ArrayList<>();
    arr.add(1);
    for (int i = 0; i < 100; ++i) {
      bt1.add(i);
      bt2.add(i);
    }
    bt1.clear();
    assertTrue(bt1.equals(bt2));

    assertFalse(bt2.removeAll(arr));
    assertTrue(bt1.equals(bt2));
    assertFalse(bt1.remove(1));
    assertTrue(bt1.equals(bt2));

    assertFalse(bt1.retainAll(arr));
    assertTrue(bt1.equals(bt2));
  }

  /* PASSES */
  @Test public void containsAllTest() {
     BTree<Integer> bt1 = new BTree<>(new ComparableComparator());
     BTree<Integer> bt2 = new BTree<>(new ComparableComparator());
    for (int i = 0; i < 100; ++i){
      bt1.add(i);
      bt2.add(i);
    }
    bt1.add(100);
    assertTrue(bt1.containsAll(bt2));
    assertFalse(bt2.containsAll(bt1));
  }
  /* PASSES */
  @Test public void iteratorCustomOrder() {
    BTree<Integer> bt = new BTree<>(13, new ComparableComparator());
    for (int i = 0; i < 400; ++i)
      bt.add(i);
    int index = 0;
    for (Integer i : bt) {
      assertTrue(i.equals(index++));
    }
    assertTrue(index == 400);
    assertTrue(bt.first().equals(0));
    assertTrue(bt.last().equals(399));
  }

  /* PASSES */
  @Test public void treeDefaultsToOrder3() {
    BTree<Integer> bt = new BTree<>(2, new ComparableComparator());
    bt.add(1);
    bt.add(2);
    bt.add(3);
    assertEquals(bt.iterator().toString(), "LeafNode: 1 2 3 ");
  }
  /* PASSES */
  @Test public void toArrayTest() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for (int i = 0; i < 100; ++i) {
      bt.add(i);
    }
    Object[] arr = bt.toArray();
    assertEquals(arr.length, 100);
    assertEquals(arr.length, bt.size());
    for (int i = 0; i < 100; ++i)
      assertTrue(arr[i].equals(i));
  }
  // PASSES
  @Test public void toArrayTestRuntime() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for (int i = 0; i < 100; ++i) {
      bt.add(i);
    }
    Integer[] arr = new Integer[100];
    bt.toArray(arr);
    for (int i = 0; i < 100; ++i)
      assertTrue(arr[i].equals(i));
  }
  // PASSES
  @Test public void toArrayTestRuntimeSmall() {
    BTree<Integer> bt = new BTree<>(new ComparableComparator());
    for (int i = 0; i < 100; ++i) {
      bt.add(i);
    }
    Integer[] arr = new Integer[10];
    arr = bt.toArray(arr);
    for (int i = 0; i < 100; ++i)
      assertTrue(arr[i].equals(i));
  }


/*  //PASSES BUT SLOW
  @Test
  public void superLargeInsertion() {
    final int SUPER_LARGE = 10000000;
    BTree<Integer> bt = new BTree<>(5, new ComparableComparator());
    for (int i = 0; i < SUPER_LARGE; ++i) {
      bt.add(i);
    }
    assertEquals(bt.size(), SUPER_LARGE);
    assertTrue(bt.first().equals(0));
    assertTrue(bt.last().equals(SUPER_LARGE - 1));
    int index = 0;
    for (Integer i : bt) {
      assertTrue(i.equals(index++));
    }
  }
  */
}
