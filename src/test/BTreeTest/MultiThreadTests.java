package BTreeTest;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.Executors;
import BTree.BTree;

public class MultiThreadTests {
	private class ComparableComparator<T extends Comparable> implements Comparator<T> {
		@Override
		public int compare(T o1, T o2) {
			return o1.compareTo(o2);
		}

		@Override
		public boolean equals(Object o) {
			return o instanceof ComparableComparator;
		}
	}

	private class FloatArrayComparator implements Comparator<float[]> {
		@Override
		public int compare(float[] o1, float[] o2) {
			if (o1.length != o2.length) {
				return o1.length < o2.length ? -1 : 1;
			}

			for (int i = 0; i < o1.length; ++i) {
				if (o1[i] == o2[i]) {
					continue;
				} else {
					if (o1[i] < o2[i]) {
						return -1;
					}
					return 1;
				}
			}
			return 0;
		}

		@Override
		public boolean equals(Object o) {
			return false;
		}
	}
    
    private class TupleComperator implements Comparator<long[]> {
		@Override
		public int compare(long[] o1, long[] o2) {
			if (o1.length != o2.length) {
				return o1.length < o2.length ? -1 : 1;
			}

			for (int i = 0; i < o1.length; ++i) {
				if (o1[i] == o2[i]) {
					continue;
				} else {
					if (o1[i] < o2[i]) {
						return -1;
					}
					return 1;
				}
			}
			return 0;
		}

		@Override
		public boolean equals(Object o) {
			return false;
		}
	}


	@Test
	public void twoThreadSearch() {
		BTree<Integer> bt = new BTree<Integer>(new ComparableComparator<Integer>());

		for (int i = 0; i < 300; ++i) {
			bt.add(i);
		}


		new Thread() {
			public void run() {
				for (int i = 299; i >= 0; --i) {
					assertEquals(true, bt.contains(i));
				}
			}
		}.start();
		new Thread() {
			public void run() {
				for (int i = 0; i < 300; ++i) {
					assertEquals(true, bt.contains(i));
				}
			}
		}.start();

	}

	@Test
	public void twoThreadInsertNoDuplicates() {
		BTree<Integer> bt = new BTree<Integer>(new ComparableComparator<Integer>());
		Thread t1 = new Thread() {
			@Override
			public void run() {
				for (int i = 0; i < 100; ++i) {
					bt.add(i);
				}
			}
		};

		Thread t2 = new Thread() {
			@Override
			public void run() {
				for (int i = 100; i < 200; ++i) {
					bt.add(i);
				}
			}
		};

		t1.start();
		t2.start();
		try {

			/* JOIN THREADS*/
			t1.join();
			t2.join();
		}catch (Exception e) {
			fail();
		}

		for (int i = 0; i < 200; ++i){
			assertEquals("i = " + i, true, bt.contains(i));
		}

		assertEquals(200, bt.size());
	}

	@Test
	public void allDuplicatesTwoThreads() {
		BTree<Integer> bt = new BTree<Integer>(new ComparableComparator<Integer>());
		Thread t1 = new Thread() {
			@Override
			public void run() {
				for (int i = 0; i < 100; ++i) {
					bt.add(i);
				}
			}
		};
		Thread t2 = new Thread() {
			@Override
			public void run() {
				for (int i = 0; i < 100; ++i) {
					bt.add(i);
				}
			}
		};

		t1.start();
		t2.start();
		try {

			/* JOIN THREADS*/
			t1.join();
			t2.join();
		}catch (Exception e) {
			fail();
		}

		for (int i = 0; i < 100; ++i){
			assertEquals("i = " + i, true, bt.contains(i));
		}

		assertEquals(100, bt.size());
	
	}

    @Test
	public void fourThreadsInsertTestWithPersistentHints() {
		BTree<Integer> bt = new BTree<Integer>(new ComparableComparator<Integer>());

		Thread t1 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = 0; i < 100; ++i) {
					bt.add(i, h);
				}
			}
		};
		Thread t2 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = 100; i < 200; ++i) {
					bt.add(i, h);
				}
			}
		};
		Thread t3 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = -100; i < 0 ; ++i) {
					bt.add(i, h);
				}
			}
		};
		Thread t4 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = -200; i < -100 ; ++i) {
					bt.add(i, h);
				}
			}
		};
        long start = System.currentTimeMillis();
		t1.start();
		t2.start();
		t3.start();
		t4.start();
        long end = 0;
		try {

			/* JOIN THREADS*/
			t1.join();
			t2.join();
			t3.join();
			t4.join();
            end = System.currentTimeMillis();
		} catch (Exception e) {
			fail();
		}

		for (int i = -200; i < 200; ++i)
			assertTrue("i = " + i, bt.contains(i));

		assertEquals(400, bt.size());
        bt.printHintStats();
        System.out.println("Persistent hints: " + (end - start));
	}

	@Test
	public void fourThreadsInsertTest() {
		BTree<Integer> bt = new BTree<Integer>(new ComparableComparator<Integer>());

		Thread t1 = new Thread() {
			@Override
			public void run() {
				for (int i = 0; i < 100; ++i) {
					bt.add(i);
				}
			}
		};
		Thread t2 = new Thread() {
			@Override
			public void run() {
				for (int i = 100; i < 200; ++i) {
					bt.add(i);
				}
			}
		};
		Thread t3 = new Thread() {
			@Override
			public void run() {
				for (int i = -100; i < 0 ; ++i) {
					bt.add(i);
				}
			}
		};
		Thread t4 = new Thread() {
			@Override
			public void run() {
				for (int i = -200; i < -100 ; ++i) {
					bt.add(i);
				}
			}
		};
        long start = System.currentTimeMillis();
		t1.start();
		t2.start();
		t3.start();
		t4.start();
        long end = 0;
		try {

			/* JOIN THREADS*/
			t1.join();
			t2.join();
			t3.join();
			t4.join();
            end = System.currentTimeMillis();
		} catch (Exception e) {
			fail();
		}

		for (int i = -200; i < 200; ++i)
			assertTrue("i = " + i, bt.contains(i));

		assertEquals(400, bt.size());
        bt.printHintStats();
        System.out.println("Non-persistent hints: " + (end - start));
	}

    @Test
    public void eightThreadInsertPersistentHints() {
		BTree<Integer> bt = new BTree<Integer>(new ComparableComparator<Integer>());
        Thread t1 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = 0; i < 100; ++i) {
					bt.add(i,h);
				}
			}
		};
		Thread t2 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = 100; i < 200; ++i) {
					bt.add(i, h);
				}
			}
		};

        Thread t3 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = bt.newHints();
                for (int i = 200; i < 300; ++i)
                    bt.add(i, h);
            }
        };
        Thread t4 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = bt.newHints();
                for (int i = 300; i < 400; ++i)
                    bt.add(i, h);
            }
        };
		Thread t5 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = -100; i < 0 ; ++i) {
                    bt.add(i, h);
				}
			}
		};
		Thread t6 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = -200; i < -100 ; ++i) {
					bt.add(i, h);
				}
			}
		};

        Thread t7 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = -300; i < -200 ; ++i) {
					bt.add(i,h);
				}
			}
		};
        Thread t8 = new Thread() {
			@Override
			public void run() {
                BTree.Hints h = bt.newHints();
				for (int i = -400; i < -300 ; ++i) {
					bt.add(i, h);
				}
			}
		};

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
            t7.join();
            t8.join();
        } catch (Exception e) {
            fail();
        }
        int index = -400;
        for (Integer i : bt) {
            assertTrue("i = " + i + " index = " + index, i.equals(index));
            index++;
        }
        assertEquals(800, bt.size());
        bt.printHintStats();
    }
    @Test
    public void eightThreadInsert() {
		BTree<Integer> bt = new BTree<Integer>(new ComparableComparator<Integer>());
        Thread t1 = new Thread() {
			@Override
			public void run() {
				for (int i = 0; i < 100; ++i) {
					bt.add(i);
				}
			}
		};
		Thread t2 = new Thread() {
			@Override
			public void run() {
				for (int i = 100; i < 200; ++i) {
					bt.add(i);
				}
			}
		};

        Thread t3 = new Thread() {
            @Override
            public void run() {
                for (int i = 200; i < 300; ++i)
                    bt.add(i);
            }
        };
        Thread t4 = new Thread() {
            @Override
            public void run() {
                for (int i = 300; i < 400; ++i)
                    bt.add(i);
            }
        };
		Thread t5 = new Thread() {
			@Override
			public void run() {
				for (int i = -100; i < 0 ; ++i) {
					bt.add(i);
				}
			}
		};
		Thread t6 = new Thread() {
			@Override
			public void run() {
				for (int i = -200; i < -100 ; ++i) {
					bt.add(i);
				}
			}
		};

        Thread t7 = new Thread() {
			@Override
			public void run() {
				for (int i = -300; i < -200 ; ++i) {
					bt.add(i);
				}
			}
		};
        Thread t8 = new Thread() {
			@Override
			public void run() {
				for (int i = -400; i < -300 ; ++i) {
					bt.add(i);
				}
			}
		};

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
            t7.join();
            t8.join();
        } catch (Exception e) {
            fail();
        }
        int index = -400;
        for (Integer i : bt) {
            assertTrue("i = " + i + " index = " + index, i.equals(index));
            index++;
        }
        assertEquals(800, bt.size());
        bt.printHintStats();
    }

    @Test
    public void random10ThreadInsert() {
        Random r = new Random(666);
        BTree<long[]> bt = new BTree<long[]>(new TupleComperator());

        Thread t1 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t2 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t3 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t4 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t5 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t6 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t7 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t8 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t9 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t10 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    bt.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();
        t10.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
            t7.join();
            t8.join();
            t9.join();
            t10.join();   
            bt.printHintStats();
        } catch (InterruptedException e) {
            fail();
        }
    }

    @Test
    public void heavyReadTest() {
        BTree<Integer> bt = new BTree<Integer>(32, new ComparableComparator());
        for (int i = 0; i < 1000; ++i)
            bt.add(i);
        
        Thread t1 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = bt.newHints();
                for (int i = 0; i < 200; ++i)
                    assertTrue(bt.contains(i, h));
            }
        };
        Thread t2 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = bt.newHints();
                for (int i = 200; i < 400; ++i)
                    assertTrue(bt.contains(i, h));
            }
        };
        Thread t3 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = bt.newHints();
                for (int i = 400; i < 600; ++i)
                    assertTrue(bt.contains(i, h));
            }
        };
        Thread t4 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = bt.newHints();
                for (int i = 600; i < 800; ++i)
                    assertTrue(bt.contains(i, h));
            }
        };
        Thread t5 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = bt.newHints();
                for (int i = 800; i < 1000; ++i)
                    assertTrue(bt.contains(i, h));
            }
        };

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
        } catch (InterruptedException e) {
            fail();
        }
        bt.printHintStats();
    }
}
