package examples;
import BTree.BTree; // Import the BTree
import java.util.*;
public class ExampleTuples {
	/**
	 * Define a Comparator that is needed to compare the elements in the BTree.
	 * This is needed both to sort, and to make sure elements are not equal in the set.
	 */
	private static class TupleComparator implements Comparator<long[]>{

		/**
		 * Compares the elements and check if they are equal!
		 * Assume that a and b are long[] of length 2
		 */
		public boolean equal(long[] a, long[] b) {
			return a[0] == b[0] && a[1] == b[1];

		}

		/**
		 * Compares the elements and returns how they should be sorted.
		 * Assume that a and b are long[] of length 2
		 * @return -1 if a < b, 0 if a == b, and 1 if a > b
		 */
		@Override
		public int compare(long[] a, long[] b) {
			if (a[0] < b[0]) {
				return -1;
			} else if (a[0] > b[0]) {
				return 1;
			}

			/* a[0] == b[0] if we get here */
			if (a[1] < b[1]) {
				return -1;
			} else if (a[1] > b[1]) {
				return 1;
			}

			/* Both are equal */
			return 0;
		}
	}
	public static void main (String[] args) {
		System.out.println("<<<<-- Example use cases of BTree for Tuple insertion! -->>>");
		
		/* Create a BTree of the correct type
		 * Make sure to specify a comparator object to be used!
		 */ 
		Comparator<long[]> comp = new TupleComparator();
		BTree<long[]> bTree = new BTree<>(comp);

		/**
		 * Create some objects to inserted into the BTree.
		 */

		long[] t1  = {1,2};
		long[] t2  = {1,3};
		long[] t3  = {8,2};
		long[] t4  = {-1,2};
		long[] t5  = {8,-1};
		long[] t6  = {12,5};
		long[] t7  = {57,2};
		long[] t8  = {-109,2};
		long[] t9  = {323,2};
		long[] t10 = {-7,2};
		long[] dup = {1,2};
		/**
		 * Add the elements using add()-method
		 * The add method will return true if the elements were added to the tree.
		 * false if they were not, for instance it was a duplicate.
		 */
		bTree.add(t1);
		bTree.add(t2);
		bTree.add(t3);
		bTree.add(t4);
		bTree.add(t5);
		bTree.add(t6);
		bTree.add(t7);
		bTree.add(t8);
		bTree.add(t9);
		bTree.add(t10);
		bTree.add(dup);

		/**
		 * Check that the duplicate was ignored by checking that size is 10 
		 */

		System.out.println("Size after all insertions: " + bTree.size());

		/**
		 * BTree is a sorted set so you can interate through the elements in its order using iterator syntax
		 */
		System.out.println("<- Elements after insertion into BTree ->");
		for (long[] el : bTree)
			System.out.println("\t [" + el[0] + ", " + el[1] + "]");

		/** 
		 * To look for elements in the BTree use the contains-method
		 */
		long[] t = {1,2};
		long[] f = {0,0};
		System.out.println("BTree contains [1,2]? " + bTree.contains(t));
		System.out.println("BTree contains [0,0]? " + bTree.contains(f));

		/**
		 * The BTree support sub, head, and tail sets
		 */
		long[] lower = {-1, 0};
		long[] upper = {8,0};	

		/**
		 * Returns a sorted set with all the elements that were present in both collections.
		 */
		SortedSet<long[]> sub = bTree.subSet(lower, upper);

		System.out.println("Elements present in both sets: " + sub.size());
		for(long[] el : sub)
			System.out.println("\t [" + el[0] + ", " + el[1] + "]");

		/**
		 * BTree can add all elements from another collection as long they are not duplicated
		 */
		ArrayList<long[]> arr = new ArrayList<>();
		long[] n1 = {-12,97};
		long[] n2 = {-42,69};
		long[] n3 = {-5,23};
		arr.add(n1);
		arr.add(n2);
		arr.add(n3);

		bTree.addAll(arr);

		System.out.println("<<- BTree size after add all : " + bTree.size() + " ->>");
		System.out.println("<<- Elements in the tree after add all ->>");
		for (long[] el : bTree)
			System.out.println("\t [" + el[0] + ", " + el[1] + "]");

		/**
		 * BTrees can be created of custom orders
		 * If no order was given in the constructor it defaults to 3.
		 */

		BTree<long[]> customOrder = new BTree<>(5, comp); 

		/**
		 * BTree is equal to another collection if they contain the same elements and the same amount of elements. 
		 */

		customOrder.add(t1);
		System.out.println("bTree compared to customOrder with only one element: " + bTree.equals(customOrder));
		/**
		 * Adding all the elements from the first BTree and comparing again 
		 */
		customOrder.addAll(bTree);
		System.out.println("customOrder BTree after addAll() : " + bTree.equals(customOrder));

	}	

}
