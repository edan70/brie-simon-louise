package BTree;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.*;
import BTree.*;
public class OptimisticReadWriteLock {
  
  /**
   * The version number utilized for the synchronization.
   *
   * Usage:
   *      - even version numbers are stable versions, not being updated
   *      - odd version numbers are temporary versions, currently being updated
   */

  private AtomicInteger atomicVersion = new AtomicInteger(0);

  public Lease startRead() {
    // Check if someone is currently writing
    while((atomicVersion.get() & 0x1) == 1) { 
      // Wait for a moment
      Thread.yield();
    }
    // done
    return new Lease(atomicVersion.get());
  }

  public boolean validate(Lease lease) {
    return lease.version == atomicVersion.get();
  }

  public boolean endRead(Lease lease) {
    return validate(lease);
  }

  public void startWrite() {
    // Invalidate all reading threads.
    int v = atomicVersion.getAndAccumulate(0x1, new IntBinaryOperator() {
        @Override
        public int applyAsInt(int x, int y) {
          return x | y;
        }
      });

    // check for concurrent writes
    while((v & 0x1) == 1) {
      Thread.yield();
      // Get updated version
      v = atomicVersion.getAndAccumulate(0x1, new IntBinaryOperator() {
        @Override
        public int applyAsInt(int x, int y) {
          return x | y;
        }
      });
    }
    // done
  }

  public boolean tryStartWrite() {
    int v = atomicVersion.getAndAccumulate(0x1, new IntBinaryOperator() {
      @Override
      public int applyAsInt(int x, int y) {
        return x | y;
      }
    });
    return !((v & 0x1) != 0); // TODO Make sure this returns the correct type
  }

  public boolean tryUpgradeToWrite(Lease lease) {
    int v = atomicVersion.getAndAccumulate(0x1, new IntBinaryOperator() {
      @Override
      public int applyAsInt(int curr, int n) {
        return curr | n;
      }
    });

    // Check whether write privileges have been gained
    if ((v & 0x1) != 0) {
	    return false;
    }
    // Check whether there was no write since the gain of the read lock
    if (v == lease.version) {
	    return true;
    }

    // if there was, undo write update
    abortWrite();

    // failed
    return false;
  }


  public void abortWrite() {
    atomicVersion.decrementAndGet(); // reset version number
  }

  public void endWrite() {
    // update version number another time
    atomicVersion.incrementAndGet();
  }

  public boolean isWriteLocked() {
    return (this.atomicVersion.intValue() & 0x1) != 0; // TODO: Check that this return correct type
  }

}
