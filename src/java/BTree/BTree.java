package BTree;
import java.util.*;
import java.lang.Math.*;
import java.io.*;
import java.util.concurrent.atomic.AtomicInteger;
public class BTree <E> implements SortedSet<E> {
    private volatile Node root = null;
    private int maxKeys;
    private volatile int searchHits = 0;
    private volatile int searchMisses = 0;
    private volatile int insertHits = 0;
    private volatile int insertMisses = 0; 
    private AtomicInteger size = new AtomicInteger(0); 
    private Comparator comp;
    private OptimisticReadWriteLock rootLock = new OptimisticReadWriteLock();
    /** Create BTree of order n
     * The order is the total number of keys in each node
     * Each node can have order + 1 children.
     * n >= 2
     */
    public BTree(int n, Comparator comp) {
        this.maxKeys = n > 3 ? n : 3; // TODO: Deviates
        this.comp = comp;
    }
    public Hints newHints() {
        return new Hints();
    }
    public void printHintStats() {
        int sumSearch = searchHits + searchMisses;
        int sumInsert = insertHits + insertMisses;
        double rateInsert = sumInsert > 0 ? (double) insertHits / (double) sumInsert : 0.0;
        double rateSearch = sumSearch > 0 ? (double) searchHits / (double) sumSearch : 0.0;

        StringBuilder sb = new StringBuilder("Hints stats:\n");
        sb.append("\tHit rate (insert)\n");
        sb.append("\t\t" + rateInsert + " Total hits: " + insertHits + " Total misses: " + insertMisses + "\n");
        sb.append("\tHit rate (search)\n");
        sb.append("\t\t" + rateSearch + " Total hits: " + searchHits + " Total misses: " + searchMisses + "\n");
        System.out.println(sb.toString());
    }

    /**
     * Create BTree of order 6
     *
     */
    public BTree(Comparator comp) {
        this.maxKeys = 3;
        this.comp = comp;
    }

    @Override
    public Comparator<? super E> comparator() {
        return this.comp;
    }


    @Override
    public E first() {
        return  ((BTreeIterator) this.iterator()).next();
    }

    @Override
    public E last() {
        return ((BTreeIterator) this.iterator()).getLastElement();
    }

    public void printTree(PrintStream out) {
        root.printTree(out, "");
    }
    @Override
    public Spliterator<E> spliterator() {
        return null;
    }

    @Override
    public SortedSet<E> subSet(E fromElement, E toElement) {
        Iterator<E> it = iterator();
        BTree<E> subSet = new BTree<E>(this.comp);
        E e = it.next();
        /*
         * Loop until we get a element in this set that is equal or greater than fromElement
         * Add to the new subset while we are less or equal to toElement.
         */

        /*
         * Inclusive
         */
        while(comp.compare(e, fromElement) < 0) {
            e = it.next();
        }

        /*
         * Exclusive
         */
        while(comp.compare(e, toElement) < 0) {
            subSet.add(e);
            e = it.next();
        }
        return subSet;
    }

    @Override
    public SortedSet<E> tailSet(E fromElement) {
        Iterator<E> it = iterator();
        BTree<E> subSet = new BTree<E>(this.comp);
        E e = it.next();
        while(comp.compare(e, fromElement) < 0) {
            e = it.next();
        }

        subSet.add(e);
        while(it.hasNext()) {
            e = it.next();
            subSet.add(e);
        }
        return subSet;

    }

    @Override
    public SortedSet<E> headSet(E toElement) {
        Iterator<E> it = iterator();
        BTree<E> subSet = new BTree<E>(this.comp);
        E e = it.next();

        while(comp.compare(e, toElement) < 0) {
            subSet.add(e);
            e = it.next();
        }
        return subSet;

    }

    public boolean add(E e) {
        Hints hints = new Hints();
        return add(e, hints);
    }

    public boolean add(E e, Hints hints) {
        // Special handling for inserting first element
        while(root == null) {
            //try to get root Lock
            if(!rootLock.tryStartWrite()){
                //sombody was faster, try again
                continue;
            }
            //check again
            if(root != null){
                rootLock.endWrite();
                break;
            }
            Node leftMost = new LeafNode();
            leftMost.numElements = 1;
            leftMost.setKey(0, e);
            //leftMost.insert(e);
            this.root = leftMost;
            this.size.incrementAndGet(); 

            rootLock.endWrite();
            hints.lastInsert = leftMost; // Add to hints

            return true;
        }

        // insert using it. imp.
        Node cur = null;
        Lease curLease = null;
        HintResult res = hints.checkHint(e);
        cur = res.hintNode;
        curLease = res.hintLease;
        // if rhere is no valid hint
        if(cur == null){
            do {// TODO: stuck here
                // get root and access lock
                Lease rootLease = rootLock.startRead();
                // start with root
                cur = root;
                //get lease of the next nnode to be accessed
                curLease = cur.lock.startRead();
                if(rootLock.endRead(rootLease)){
                    break;
                }
            } while(true);
        } 

        while (true) {
            if (cur.isInner()) {
                // E a = cur.getKey(0);
                //E b = cur.getKey(cur.getNumElements());
                int pos = cur.lowerBound(e, 0, cur.numElements); // TODO: Assume this works for now.
                int idx = pos; // TODO: Weird

                // Duplicate check?
                if (!(pos >= cur.numElements) && comp.compare(cur.getKey(pos), e) == 0) { 
                    return false; 
                }
                //System.out.println("root: " + cur + " IDX : " + idx);
                Node next = cur.getChild(idx);
                Lease nextLease = next.lock.startRead(); //  FIXME: Sometimes gives NullPointerException. How? Next shouldn't be null?
                // And lock even less so.

                //Check if there was a writes
                if(!cur.lock.endRead(curLease)){
                    //start over
                    return add(e, hints);
                }
                cur = next;
                curLease = nextLease;
                //System.out.println("CUR: " + cur);
                continue; //syfte?
            }
            //E a = cur.getKey(0);
            //E b = cur.getKey(cur.numElements);

            int pos = cur.upperBound(e, 0, cur.numElements);
            int idx = pos; // TODO: Weird

            if (pos != 0 && comp.compare(cur.getKey(pos - 1), e) == 0) {
                return false;
            }

            if(!cur.lock.tryUpgradeToWrite(curLease)){
                // something has changed, restart
                hints.lastInsert = cur;
                return add(e, hints); 
            }

            if (cur.numElements >= maxKeys) {
                //System.out.println("Splitting node!");
                // split node
                Node priv = cur;
                Node parent = priv.parent;
                Vector<Node> parents = new Vector<>();
                do{
                    if(parent != null){
                        parent.lock.startWrite();
                        while(true){
                            //check if parent is correct
                            if(parent == priv.parent){
                                break;
                            }
                            //switch parent
                            parent.lock.abortWrite();
                            parent = priv.parent;
                            parent.lock.startWrite();
                        }
                    } else {
                        //Lock root lock since cur is Root
                        rootLock.startWrite();
                    }
                    parents.add(parent);  // TODO: Deviates alot.
                    if(parent == null || !(parent.numElements >= maxKeys)){
                        break;
                    }
                    priv = parent;
                    parent = parent.parent;
                } while(true);

                // split this node
                Node oldRoot = root;
                idx -= cur.rebalanceOrSplit(idx, parents);

                //realease parent Lock
                for (int i = parents.size() - 1; i >= 0; --i) {
                    Node par = parents.get(i);
                    if(par != null){
                        par.lock.endWrite();
                    }else{
                        if (oldRoot != root){
                            rootLock.endWrite();
                        } else {
                            rootLock.abortWrite();
                        }
                    } 

                }
                // insert element in right fragment
                if (idx > cur.numElements) {
                    cur.lock.endWrite();
                    return add(e, hints);
                }
            }

            for (int j = cur.numElements; j > idx; --j) {
                cur.setKey(j, cur.getKey(j - 1));
            }
            // Insert new elements
            cur.setKey(idx, e);
            ++(cur.numElements);

            //this.size++;
            this.size.incrementAndGet();
            cur.lock.endWrite();
            hints.lastInsert = cur; // adds new element as last Inserted in hints.
            return true;
        }
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean addedSomething = false;
        for (E e : c) {
            if (this.add(e)) {
                addedSomething = true;
            }
        }
        return addedSomething;
    }

    @Override
    public void clear() {
        return;
    }

    @Override
    public boolean contains(Object o) {
        Hints h = new Hints();
        return find(o, h) != null;
    }

    public boolean contains(Object o, Hints h) {
        return find(o, h) != null;
    }


    /**
     * TODO: Should BTree crash if contains is called type != E?
     */
    private E find(Object key, Hints hints) {
        if (isEmpty()){
            return null;
        }
        Node cur = root;
        if(hints.checkHintSearch((E) key)){
            cur = hints.lastFind;
        }

        while(true) {
            int pos = cur.search((E) key, 0, cur.numElements);
            if (pos < cur.numElements && comp.compare(cur.getKey(pos),key) == 0){
                hints.lastFind = cur;
                return cur.getKey(pos);
            }

            if (!cur.isInner) {
                hints.lastFind = cur;
                return null;
            }
            cur = cur.getChild(pos);
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object k : c) {
            if (!this.contains(k)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Equals returns true iff o is a BTree<E>, same size, contains the same elements, and uses the same kind of comparator.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof BTree) {
            BTree<E> other = (BTree) o;
            for (E e : other) {
                if (!this.contains(e)) {
                    return false;
                }
            }

            return this.comp.equals(other.comparator()) && this.size.get() == other.size();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return this.size.get() == 0;
    }

    /* Create iterator that iterates through objects in sorted order */
    @Override
    public Iterator<E> iterator() {
        return new BTreeIterator(this);
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public int size() {
        return this.size.get();
    }

    @Override
    public Object[] toArray() {
        Object[] arr = new Object[this.size.get()];
        int index = 0;
        for (Object e : this) {
            arr[index++] = e;
        }
        return arr;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length >= this.size.get()) {
            int index = 0;
            for (Object t : this) {
                a[index++] = (T) t;
            }

            return a;
        }
        Object[] larger = new Object[this.size.get()];
        int index = 0;
        for (Object t : this) {
            larger[index++] = t;
        }
        return (T[]) Arrays.copyOf(larger, larger.length, a.getClass());
    }


    /** Private base class for all Nodes.
     *  Does not contain any keys or child nodes.
     *  TODO: Can be made abstract?
     * */
    private class Base {
        public volatile InnerNode parent;
        // number of keys in this node
        public volatile int numElements; // TODO: Change to atomic?
        public OptimisticReadWriteLock lock = new OptimisticReadWriteLock();
        // Pos. in parent node
        public volatile int position;
        public volatile boolean isInner;
        public Base(boolean isInner) {
            this.isInner = isInner;
            this.parent = null;
            this.position = 0;
            this.numElements = 0;
        }

        public boolean isLeaf() {
            return !this.isInner;
        }

        public boolean isInner() {
            return this.isInner;
        }

        public int getNumElements() {
            return numElements;
        }
    } // End of Base-class

    /**
     * A basic Node.
     * Covers all operations for inner and leaf nodes.
     * Every Node in a BTree must contains at most order children.
     * Every node in a BTree except the root node and the leaf nodes contain at least order / 2 children
     * The root nodes must have at least 2 nodes.
     * All leaf nodes must be at the same level.
     */
    public abstract class Node extends Base {
        //
        //public List<E> keys = new ArrayList<>(maxKeys);
        Object[] keys = new Object[maxKeys];
        public Node(boolean isInner) {
            super(isInner);
        }

        /*
         * Simple linear search for now.
         */
        public int search(E key, int start, int end) {
            return lowerBound(key, start, end);
        }

        public void setKey(int index, E k) {
            keys[index] = k;
            //keys.set(index, k);
        }

        public E getKey(int index) {
            //return this.keys.get(index);
            return (E) this.keys[index];
        }
        /**
         * compares the values of aand b and returns:
         * -1 if a < b
         *  1 if a > b
         *  0 o/w
         */
        private int compare(E a, E b) {
            int res = comp.compare(a,b);
            if (res < 0) {
                return -1;
            } else if (res > 0) {
                return 1;
            }
            return 0;
        }
        /**
         * BINARY SEARCH
         * Obtains an index for the reference to the first element in the given range that is not less than the given key.
         */
        public int lowerBound(E k, int a, int b) {
            int c;
            int count = b - a;
            while (count > 0) {
                int step = count >> 1;
                c = a + step;
                int r = comp.compare(getKey(c), k);

                if (comp.compare(getKey(c), k) < 0) {
                    a = ++c;
                    count -= step + 1;
                } else {
                    count = step;
                }
            }
            return a;
        }

        public int upperBound(E k, int a, int b) {
            int c;
            int count = b - a;
            while (count > 0) {
                int step = count >> 1;
                c = a + step;
                if (comp.compare(k, getKey(c)) >= 0) {
                    a = ++c;
                    count -= step + 1;
                } else {
                    count = step;
                }
            }
            return a;
        }


        public void printTree(PrintStream out, String prefix) {
            out.print(prefix + "@" + this  + "[" + this.position + "] - "
                    + (this.isInner ? "i" : "") + "node: " + this.numElements + "/" + maxKeys + "[");

            for (int i = 0; i < this.numElements; i++){
                out.print(getKey(i));
                if (i != this.numElements - 1) {
                    out.print(",");
                }
            }

            out.print("]");

            if (this.isInner) {
                out.print(" - [");
                for (int i = 0; i < this.numElements; i++) {
                    out.print(getChild(i));
                    if (i != this.numElements) {
                        out.print(",");
                    }
                }
                out.print("]");
            }
            out.print("\n");

            if (this.isInner) {
                for (int i = 0; i < this.getNumChildren(); ++i) {
                    this.getChild(i).printTree(out, prefix + "    ");
                }
            }
        }

        /*
           public abstract int countNodes();
           public abstract int countEntries();
           public abstract int getDepth();
           */
        public abstract Node getChild(int i); // TODO: Weird
        public abstract String toString();
        public abstract int getNumChildren();

        /**
         * Obtains the point where full nodes should be split.
         * Conventionaly BTrees are split in the middle.
         * For a high frequency of in-order insertions, a split assigning larger
         * portions of to the right fragment provide higher performance
         * and a better node-filling rate.
         */
        public int getSplitPoint() {
            //return (int) Math.ceil(maxKeys / 2);
            return Math.min(3 * maxKeys / 4, maxKeys - 2);
        }

        public void split(int index, Vector<Node> lockedNodes) {
            int splitPoint  = getSplitPoint();
            // Create new node
            Node sibling = this.isInner ? new InnerNode() : new LeafNode();

            sibling.lock.startWrite();
            lockedNodes.add(sibling);

            // Move data over to the new node
            for (int i = splitPoint + 1, j = 0; i < maxKeys; ++i, ++j) {
                //sibling.setKey(j, this.keys.get(i));
                sibling.setKey(j, this.getKey(i));
            }

            // move child pointers
            if (this.isInner) {
                InnerNode me = (InnerNode) this;

                // move pointers to sibling
                InnerNode other = (InnerNode) sibling; // TODO: Copy by value or by reference?
                for (int i = splitPoint + 1, j = 0; i <= maxKeys; ++i, ++j) {
                    other.setChild(j, me.getChild(i)); // TODO: Deviates, maybe due to temporal and/or spatial locality.
                    other.getChild(j).parent = other;
                    other.getChild(j).position = j;
                    //me.setChild(i, null); // FIXME: This caused bugs! 
                }
            }
            this.numElements = splitPoint;
            sibling.numElements = maxKeys - splitPoint - 1;
            growParent(sibling, lockedNodes);
        }

        public int rebalanceOrSplit(int index, Vector<Node> lockedNodes) {
            InnerNode parent = this.parent;
            int pos = this.position;

            // Option A, rebalance node
            if (parent != null && pos > 0) { // TODO: Deviates?
                Node left = parent.getChild(pos - 1);

                if(!left.lock.tryStartWrite()){
                    //TODO: deviates.
                    //left node is currently updated ?? skip balancing and split
                    split(index, lockedNodes);
                    return 0;
                }

                int num = (int) Math.min(maxKeys - left.numElements, index);
                if (num > 0) {
                    E splitter = parent.getKey(this.position - 1);
                    left.setKey(left.numElements, splitter);
                    //left.setKey(left.getNumElements(), parent.getKey(this.position - 1));
                    int i;
                    for (i = 0; i < num - 1; ++i) {
                        left.setKey(left.getNumElements() + 1 + i, this.getKey(i));
                    }
                    splitter = this.getKey(num - 1); //Borde vi sätta den i parent?
                    parent.setKey(this.position - 1, splitter);

                    for (i = 0; i < this.getNumElements() - num; ++i) {
                        this.setKey(i, this.getKey(i + num)); // TODO: Deviates
                    }

                    if (this.isInner()) {
                        InnerNode iLeft = (InnerNode) left;
                        InnerNode iRight = (InnerNode) this;
                        // Move children
                        for (i = 0; i < num; ++i) {
                            iLeft.setChild(left.numElements + i + 1, iRight.getChild(i)); // TODO: Deviates
                            iRight.getChild(i).parent = iLeft;
                            iRight.getChild(i).position = left.getNumElements() + i + 1;
                        }
                        // update moved children
                        // TODO Changed here to Optimize:
                        /*
                           for (i = 0; i < num; ++i) {
                           iRight.getChild(i).parent = iLeft;
                           iRight.getChild(i).position = left.getNumElements() + i + 1;
                           }
                           */
                        for (i = 0; i < this.numElements - num + 1; ++i) {
                            iRight.setChild(i, this.getChild( i + num));
                            iRight.getChild(i).position = i;
                        }
                        /*
                        // TODO: OPTIMIZATION Mash the above loop together with this one.
                        for (i = 0; i < this.getNumElements() - num + 1; ++i) {
                        iRight.getChild(i).position = i;
                        }
                        */
                    }
                    left.numElements += num;
                    this.numElements -= num;

                    left.lock.endWrite();
                    // done
                    return num;
                }
                left.lock.abortWrite();
            }
            // Option B, split node.
            split(index,lockedNodes);
            return 0; // No rebalance
        }


        private void growParent(Node sibling, Vector<Node> lockedNodes) {
            if (this.parent == null) {
                // Create a new root node
                InnerNode newRoot = new InnerNode();
                newRoot.numElements = 1;
                newRoot.setKey(0, this.getKey(this.numElements));

                newRoot.setChild(0, this);
                newRoot.setChild(1, sibling);

                // link this and the sibling node to new root
                this.parent = newRoot;
                sibling.parent = newRoot;
                sibling.position = 1;

                // switch root node
                root = newRoot;
            } else {
                InnerNode parent = this.parent;
                int pos = this.position;
                parent.insertInner(pos, this, getKey(this.numElements), sibling, lockedNodes);
            }
        }
    } // End of LeafNode class

    /***
     * InnerNode is a Node which has children nodes.
     */
    private class InnerNode extends Node {
        //public volatile List<Node> children = new ArrayList<>(maxKeys + 1);
        public volatile Object[] children =  new Object[maxKeys + 1]; 
        public InnerNode() {
            super(true);
            // Necessary because ArrayList
        }

        public void setChild(int index, Node n) {
            //this.children.set(index, n);
            this.children[index] = n;
        }

        public Node getChild(int index) {
            return (Node) this.children[index];
        }

        public int getNumChildren() {
            return numElements + 1;
        }

        public void insertInner(int pos, Node predecessor, E key, Node newNode, Vector<Node> lockedNodes) {
            if (this.numElements >= maxKeys) {
                // split this node
                pos -= rebalanceOrSplit(pos, lockedNodes);
                if (pos > this.numElements) {
                    // correct position
                    pos = pos - this.numElements - 1;
                    // get new sibling
                    InnerNode other = (InnerNode) this.parent.getChild(this.position + 1); // TODO: Deviates make sure type is correct!
                    //InnerNode other = this.parent.getChild(this.position + 1); // TODO: Deviates make sure type is correct!
                    int i = 0;
                    for(i = 0 ; i <= other.numElements; i++){
                        if(other.getChild(i) == predecessor){
                            break;
                        }
                    }

                    pos = (i > other.numElements)? 0: i;

                    other.insertInner(pos, predecessor, key, newNode, lockedNodes);
                    return;
                }
            }
            // move bigger keys one forward
            for (int i = this.numElements - 1; i >= pos; --i) {
                this.setKey(i + 1, getKey(i));
                setChild(i + 2, getChild(i + 1)); // TODO: Deviates why load full array?
                getChild(i + 2).position = getChild(i + 2).position + 1;
            }

            setKey(pos, key);
            setChild(pos + 1, newNode);
            newNode.parent = this;
            newNode.position = pos + 1;
            ++(this.numElements);
        }

        @Override
        public String toString() {
            return "InnerNode";
        }
    }

    private class LeafNode extends Node {
        public LeafNode() {
            super(false);
        }

        @Override
        public int getNumChildren() {
            return 0;
        }
        @Override
        public Node getChild(int i) {
            assert 1 == 0: "Don't call getChild on LeafNodes!";
            return null;
        }
        @Override
        public String toString() {
            return "LeafNode";
        }
    }
    /**
     * A collection of operation hints speeding up some of the involved operations
     * by exploiting temporal locality.
     */
    public class Hints{
        public volatile Node lastFind = null; //The node where the last find terminated
        public volatile Node lastInsert = null; //The node where the last insertion terminated
        public HintResult checkHint(E k){
            if(lastInsert != null){  // Ignore null pointer
                Lease hintLease = lastInsert.lock.startRead();
                //  Lease hintLease = n.lock.startRead();
                if(!covers(lastInsert, k)){ // does it cover the key
                    insertMisses++;
                    return new HintResult(null, null);
                }
                if(!lastInsert.lock.validate(hintLease)){
                    insertMisses++;
                    return new HintResult(null, null);
                }
                insertHits++;
                return new HintResult(lastInsert, hintLease);
            }
            //insertMisses++; First search don't add
            return new HintResult(null, null);
        }
        public boolean checkHintSearch(E k){
            if (lastFind == null) {
                // searchMisses++; First search don't add
                return false;
            }

            if(!covers(lastFind, k)){ // does it cover the key
                searchMisses++;
                return false;
            }
            searchHits++;
            return true;
        }
        /**
         * Determines weather the range covered by the given node is also
         * covering the given key value
         */
        public boolean covers(Node n, E k) {
            return weakLess(n.getKey(0), k) && weakLess(k, n.getKey(n.numElements - 1));
        }

        public boolean weakCovers(Node n, E k) {
            return n.numElements != 0 && !weakLess(k, n.getKey(0)) && !weakLess(n.getKey(n.numElements - 1), k);
        }

        public boolean weakLess(E a, E b){
            return comp.compare(a,b) < 0;
        }
    }


    private class BTreeIterator implements Iterator {
        private Node curNode = null; // Current node
        private Node lastNode = null; // Last node in the tree. Used to stop iteration.
        private boolean isLeaf = false;
        private Stack<Integer> vKeysStack = new Stack<>();
        private Stack<boolean[]> vChildStack = new Stack<>();
        private boolean printPath = false;
        public BTreeIterator(BTree<E> tree) {
            this.curNode = tree.root;
            this.lastNode = tree.root;
            setFirstNode();
            setLastNode();
        }


        /**
         * Returns true if there are more elements to iterate through.
         */
        @Override
        public boolean hasNext() {
            return !(curNode == lastNode && vKeysStack.peek() == curNode.numElements);
        }

        /**
         * Finds the left-most node in the tree or subtree of an inner node and sets current node to it.
         */
        private void setFirstNode() {
            // Base case, we have found the leaf node.
            if(curNode.isLeaf()) {
                this.isLeaf = true;
                vKeysStack.push(0); // Push the number of visited children in this node.
                return;
            }

            // Add values to stack representing the inner node
            vKeysStack.push(0);
            vChildStack.push(new boolean[curNode.numElements + 1]);
            vChildStack.peek()[0] = true;
            // We are in a inner node, got to left most child in node
            curNode = curNode.getChild(0);
            setFirstNode(); // Recursive call
        }

        private void setLastNode() {
            // Base case, we have found the right-most leaf node.
            if (lastNode.isLeaf()) {
                return;
            }

            // We are in an inner node, go to the right-most child in this node.
            lastNode = lastNode.getChild(lastNode.numElements);
            setLastNode(); // Recursive call.
        }

        /**
         * Returns the next element in the iteration.
         */
        @Override
        public E next() {
            /*
             * If we have not visited all keys in this node take the next key.
             *
             * Else:
             * Pop the current info from the stack, it is not useful anymore
             * Switch to the parent node
             * call next() again to make the parent node do its thing
             */
            if (isLeaf) {
                // If we have not visited all in leaf node, do so first
                if (vKeysStack.peek() < curNode.numElements) {
                    int curVal = vKeysStack.peek();
                    E ret = curNode.getKey(curVal);
                    vKeysStack.pop();
                    vKeysStack.push(curVal + 1);
                    return ret;
                }


                // We have visited all children in leaf node
                // Switch to parent node
                curNode = curNode.parent;
                this.isLeaf = curNode.isLeaf();
                vKeysStack.pop();
                // Now parent node is curNode, call next again.
                return next();
            } else { // We are in a inner node

                /*
                 * If we are at an index where there is an unvisited child
                 *    switch current node to the node that corresponds to that index
                 *    go to the left most child node in the subtree
                 *    call next() again to process on the new leaf node
                 *
                 */

                if (atUnvisitedChildIndex()) {
                    curNode = curNode.getChild(vKeysStack.peek());
                    vChildStack.peek()[vKeysStack.peek()] = true;
                    setFirstNode();
                    return next();
                }

                if(visitedAllChildren()) {
                    // Move up in tree
                    vKeysStack.pop();
                    vChildStack.pop();
                    curNode = curNode.parent;
                    return next();
                }
                // If we have iterated to a child splitpoint and we haven't already visited that child
                int curVal = vKeysStack.peek();
                E ret = curNode.getKey(curVal);
                vKeysStack.pop();
                vKeysStack.push(curVal + 1);
                return ret;
            }
        }

        public E getLastElement(){
            return lastNode.getKey(lastNode.numElements - 1);
        }

        private boolean visitedAllChildren() {
            for (int i = 0; i < curNode.numElements + 1; ++i) {
                if (!vChildStack.peek()[i]) {
                    return false;
                }
            }
            return true;
        }
        /**
         *
         */
        private boolean atUnvisitedChildIndex() {
            Integer i = vKeysStack.peek();
            return !vChildStack.peek()[i];
        }

        /**
         * Undefined for this BTree
         */
        @Override
        public void remove() {
            return;
        }

        /**
         * toString method for debugging purposes.
         */
        @Override
        public String toString() {
            StringBuilder b = new StringBuilder(curNode.toString() + ": ");
            for (int i = 0; i < curNode.numElements; ++i) {
                b.append(curNode.getKey(i) + " ");
            }
            return b.toString();
        }
    }

    private void update(E old, E nu) {
        /* Do nothing for now */
    }

    private class HintResult {
        public Node hintNode;
        public Lease hintLease;

        public HintResult (Node n, Lease l) {
            this.hintNode = n;
            this.hintLease = l;
        }
    }
}
