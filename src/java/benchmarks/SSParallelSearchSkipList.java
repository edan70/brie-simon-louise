package benchmarks;
import java.util.*; 
import java.util.concurrent.*; 
public class SSParallelSearchSkipList {
	private static class TupleComparator implements Comparator<long[]>{
		public boolean equal(long[] a, long[] b) {
			return a[0] == b[0] && a[1] == b[1];
		}
		@Override
		public int compare(long[] a, long[] b) {
			if (a[0] < b[0]) {
				return -1;
			} else if (a[0] > b[0]) {
				return 1;
			}
			if (a[1] < b[1]) {
				return -1;
			} else if (a[1] > b[1]) {
				return 1;
			}
			return 0;
		}
	}
    private static ConcurrentSkipListSet<long[]> bt = new ConcurrentSkipListSet<long[]>(new TupleComparator());

    public static void setup() {
        for (long i = 0; i < 10000; ++i)
            for (long k = 0; k < 2000; ++k)
                bt.add(new long[] {i,k});
    }

    public static void run() {
        Thread t1 = new Thread() {
            @Override 
            public void run() {
                for (long i = 0; i < 2000; ++i)
                    for (long k = 0; k < 2000; ++k)
                        bt.contains(new long[] {i,k});
            }
        };
        Thread t2 = new Thread() {
            @Override 
            public void run() {
                for (long i = 2000; i < 4000; ++i)
                    for (long k = 0; k < 2000; ++k)
                        bt.contains(new long[] {i,k});
            }
        };
        Thread t3 = new Thread() {
            @Override 
            public void run() {
                for (long i = 4000; i < 6000; ++i)
                    for (long k = 0; k < 2000; ++k)
                        bt.contains(new long[] {i,k});
            }
        };
        Thread t4 = new Thread() {
            @Override 
            public void run() {
                for (long i = 6000; i < 8000; ++i)
                    for (long k = 0; k < 2000; ++k)
                        bt.contains(new long[] {i,k});
            }
        };
        Thread t5 = new Thread() {
            @Override 
            public void run() {
                for (long i = 8000; i < 10000; ++i)
                    for (long k = 0; k < 2000; ++k)
                        bt.contains(new long[] {i,k});
            }
        };
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
