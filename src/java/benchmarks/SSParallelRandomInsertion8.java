package benchmarks;
import BTree.BTree;
import java.util.*;
public class SSParallelRandomInsertion8 {
    private static class TupleComparator implements Comparator<long[]>{
		public boolean equal(long[] a, long[] b) {
			return a[0] == b[0] && a[1] == b[1];
		}

		@Override
		public int compare(long[] a, long[] b) {
			if (a[0] < b[0]) {
				return -1;
			} else if (a[0] > b[0]) {
				return 1;
			}
			if (a[1] < b[1]) {
				return -1;
			} else if (a[1] > b[1]) {
				return 1;
			}
			return 0;
		}
	}
    
    public static void run() {
        Random r = new Random(666);
        BTree<long[]> set = new BTree<>(8, new TupleComparator());
        Thread t1 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 4000000; ++i)
                    set.add(new long[] {r.nextLong(), r.nextLong()}, h);
            }
        };
        Thread t2 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int j = 0; j < 4000000; ++j)
                    set.add(new long[] {r.nextLong(), r.nextLong()}, h);
            }
        };
        Thread t3 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int j = 0; j < 4000000; ++j)
                        set.add(new long[] {r.nextLong(), r.nextLong()}, h);
            }
        };

        Thread t4 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 4000000; ++i)
                    set.add(new long[] {r.nextLong(), r.nextLong()}, h);
            }
        };

        Thread t5 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 4000000; ++i)
                    set.add(new long[] {r.nextLong(), r.nextLong()}, h);
            }
        };

        Thread t6 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int j = 0; j < 4000000; ++j)
                    set.add(new long[] {r.nextLong(), r.nextLong()}, h);
            }
        };
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
