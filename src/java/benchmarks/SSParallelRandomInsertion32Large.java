package benchmarks;
import BTree.BTree;
import java.util.*;
public class SSParallelRandomInsertion32Large {
    private static class TupleComparator implements Comparator<long[]>{
        public boolean equal(long[] a, long[] b) {
            return compare(a, b) == 0; 
        }
        @Override
        public int compare(long[] a, long[] b) {
            // Assume equal length
            for (int i = 0; i < a.length; ++i) {
                if (a[i] < b[i])
                    return -1;
                if (a[i] > b[i])
                    return 1;
            }

            return 0;
        }
    }    
    public static void run() {
        Random r = new Random(666);
        BTree<long[]> set = new BTree<>(32, new TupleComparator());
        Thread t1 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 4000000; ++i) {
                    long[] tuple = new long[] {r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(),
                        r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong()};
                    set.add(tuple, h);
                }
            }
        };
        Thread t2 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 4000000; ++i) {
                    long[] tuple = new long[] {r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(),
                        r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong()};
                    set.add(tuple, h);
                }
            }
        };
        Thread t3 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 4000000; ++i) {
                    long[] tuple = new long[] {r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(),
                        r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong()};
                    set.add(tuple, h);
                }
            }
        };
        Thread t4 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 4000000; ++i) {
                    long[] tuple = new long[] {r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(),
                        r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong()};
                    set.add(tuple, h);
                }
            }
        };
        Thread t5 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 4000000; ++i) {
                    long[] tuple = new long[] {r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(),
                        r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong()};
                    set.add(tuple, h);
                }
            }
        };
        Thread t6 = new Thread() {
            @Override
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 4000000; ++i) {
                    long[] tuple = new long[] {r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(),
                        r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong(), r.nextLong()};
                    set.add(tuple, h);
                }
            }
        };
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
