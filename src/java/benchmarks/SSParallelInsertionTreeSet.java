package benchmarks;
import java.util.Comparator;
import java.util.*;
import java.util.concurrent.*;
public class SSParallelInsertionTreeSet {
    private static class TupleComparator implements Comparator<long[]>{
		public boolean equal(long[] a, long[] b) {
			return a[0] == b[0] && a[1] == b[1];

		}
		@Override
		public int compare(long[] a, long[] b) {
			if (a[0] < b[0]) {
				return -1;
			} else if (a[0] > b[0]) {
				return 1;
			}
			if (a[1] < b[1]) {
				return -1;
			} else if (a[1] > b[1]) {
				return 1;
			}
			return 0;
		}
	}
    
    public static void run() {
        SortedSet<long[]> set = Collections.synchronizedSortedSet(new TreeSet<>(new TupleComparator()));
        Thread t1 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; ++i)
                    for (int j = 0; j < 4000; ++j)
                        set.add(new long[] {i, j});
            }
        };
        Thread t2 = new Thread() {
            @Override
            public void run() {
                for (int i = 1000; i < 2000; ++i)
                    for (int j = 0; j < 4000; ++j)
                        set.add(new long[] {i,j});
            }
        };
        Thread t3 = new Thread() {
            @Override
            public void run() {
                for (int i = 2000; i < 3000; ++i)
                    for (int j = 0; j < 4000; ++j)
                        set.add(new long[] {i,j});
            }
        };

        Thread t4 = new Thread() {
            @Override
            public void run() {
                for (int i = 3000; i < 4000; ++i)
                    for (int j = 0; j < 4000; ++j)
                        set.add(new long[] {i,j});
            }
        };

        Thread t5 = new Thread() {
            @Override
            public void run() {
                for (int i = 4000; i < 5000; ++i)
                    for (int j = 0; j < 4000; ++j)
                        set.add(new long[] {i,j});
            }
        };

        Thread t6 = new Thread() {
            @Override
            public void run() {
                for (int i = 5000; i < 6000; ++i)
                    for (int j = 0; j < 4000; ++j)
                        set.add(new long[] {i,j});
            }
        };
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
