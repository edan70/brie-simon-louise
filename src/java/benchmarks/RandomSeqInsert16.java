package benchmarks;
import BTree.BTree;
import java.util.Comparator;
import java.util.Random;
public class RandomSeqInsert16 {
    private static class TupleComparator implements Comparator<long[]>{
        public boolean equal(long[] a, long[] b) {
            return a[0] == b[0] && a[1] == b[1];

        }
        @Override
        public int compare(long[] a, long[] b) {
            if (a[0] < b[0]) {
                return -1;
            } else if (a[0] > b[0]) {
                return 1;
            }
            if (a[1] < b[1]) {
                return -1;
            } else if (a[1] > b[1]) {
                return 1;
            }
            return 0;
        }
    }


    public static void run() {
        Random r = new Random(666);
        BTree<long[]> set = new BTree<>(16, new TupleComparator());
        BTree.Hints h = set.newHints();
        for (int i = 0; i < 10000; ++i)
            set.add(new long[] {r.nextLong(), r.nextLong()}, h);
    }
}
