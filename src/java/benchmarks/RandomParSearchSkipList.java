package benchmarks;
import java.util.Random;
import java.util.*;
import java.util.concurrent.*;

public class RandomParSearchSkipList {
    private static class TupleComparator implements Comparator<long[]>{
        public boolean equal(long[] a, long[] b) {
            return a[0] == b[0] && a[1] == b[1];

        }
        @Override
        public int compare(long[] a, long[] b) {
            if (a[0] < b[0]) {
                return -1;
            } else if (a[0] > b[0]) {
                return 1;
            }
            if (a[1] < b[1]) {
                return -1;
            } else if (a[1] > b[1]) {
                return 1;
            }
            return 0;
        }
    }

    private static Set<long[]> unorderedSet = new HashSet<long[]>();
    private static ConcurrentSkipListSet<long[]> set = new ConcurrentSkipListSet<long[]>(new TupleComparator());

    public static void setup() {
        Random r = new Random(666);
        for (int i = 0; i < 10000; ++i) {
            long[] pair = new long[] {r.nextLong(), r.nextLong()};
            unorderedSet.add(pair);
            set.add(pair);
        }

    }

    public static void run() {
        long[][] arr = (long[][]) unorderedSet.toArray();
        int arrLen  = arr.length;
        int sixth   = arrLen / 6;

        Thread t1 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < sixth; ++i)
                    set.contains(arr[i]);
            }
        };

        Thread t2 = new Thread() {
            @Override
            public void run() {
                for (int i = sixth; i < 2 * sixth; ++i)
                    set.contains(arr[i]);
            }
        };

        Thread t3 = new Thread() {
            @Override
            public void run() {
                for (int i = 2 * sixth; i < 3 * sixth; ++i)
                    set.contains(arr[i]);
            }
        };
        Thread t4 = new Thread() {
            @Override
            public void run() {
                for (int i = 3 * sixth; i < 4 * sixth; ++i)
                    set.contains(arr[i]);
            }
        };

        Thread t5 = new Thread() {
            @Override
            public void run() {
                for (int i = 4 * sixth; i < 5 * sixth; ++i)
                    set.contains(arr[i]);
            }
        };

        Thread t6 = new Thread() {
            @Override
            public void run() {
                for (int i = 5 * sixth; i < 6 * sixth; ++i)
                    set.contains(arr[i]);
            }
        };

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
