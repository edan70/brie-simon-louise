package benchmarks;
import java.util.*;
public class SequentialInsertionTreeSet {
    private static class TupleComparator implements Comparator<long[]> {
        public boolean equal (long[] a, long[] b) {
            return a[0] == b[0] && a[1] == b[1];
        }

       @Override
       public int compare (long[] a, long[] b) {
            if (a[0] < b[0]) {
                return -1;
            } else if(a[0] > b[0]) {
                return 1;
            }
            if (a[1] < b[1]) {
                return -1;
            } else if (a[1] > b[1]) {
                return 1;
            }
            return 0;
        }
    }
    public static void run() {
        SortedSet ts = Collections.synchronizedSortedSet(new TreeSet<long[]>(new TupleComparator()));
        for (long i = 0; i < 100; ++i)
            for (long k = 0; k < 100; ++k)
                ts.add(new long[] {i,k});
    }
}
