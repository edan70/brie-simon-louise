package benchmarks;
import BTree.BTree;
import java.util.*;
import java.util.concurrent.*;
public class RandomSeqInsertTreeSet {
    private static class TupleComparator implements Comparator<long[]>{
        public boolean equal(long[] a, long[] b) {
            return a[0] == b[0] && a[1] == b[1];

        }
        @Override
        public int compare(long[] a, long[] b) {
            if (a[0] < b[0]) {
                return -1;
            } else if (a[0] > b[0]) {
                return 1;
            }
            if (a[1] < b[1]) {
                return -1;
            } else if (a[1] > b[1]) {
                return 1;
            }
            return 0;
        }
    }


    public static void run() {
        Random r = new Random(666);
        SortedSet<long[]> set = Collections.synchronizedSortedSet(new TreeSet<>(new TupleComparator()));
        for (int i = 0; i < 10000; ++i)
            set.add(new long[] {r.nextLong(), r.nextLong()});
    }
}
