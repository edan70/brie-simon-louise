package benchmarks;
import java.util.Comparator;
import java.util.*;
import java.util.concurrent.*;

public class RandomSeqSearchSkipList {
    private static class TupleComparator implements Comparator<long[]>{
        public boolean equal(long[] a, long[] b) {
            return a[0] == b[0] && a[1] == b[1];

        }
        @Override
        public int compare(long[] a, long[] b) {
            if (a[0] < b[0]) {
                return -1;
            } else if (a[0] > b[0]) {
                return 1;
            }
            if (a[1] < b[1]) {
                return -1;
            } else if (a[1] > b[1]) {
                return 1;
            }
            return 0;
        }
    }

    private static Set<long[]> unorderedSet = new HashSet<long[]>();
    private static ConcurrentSkipListSet<long[]> set = new ConcurrentSkipListSet<>(new TupleComparator());
    public static void setup() {
        Random r = new Random(666);
        long a, b;
        for (int i = 0; i < 10000; ++i) {
            long[] pair = new long[] {r.nextLong(), r.nextLong()};
            unorderedSet.add(pair);
            set.add(pair);
        }

    }

    public static void run() {
        for (long[] pair : unorderedSet)
            set.contains(pair);
    }
}
