package benchmarks;
import BTree.BTree;
import java.util.*;

public class ParallelSearch8 {
    private static class TupleComparator implements Comparator<long[]>{
        public boolean equal(long[] a, long[] b) {
            return a[0] == b[0] && a[1] == b[1];

        }
        @Override
        public int compare(long[] a, long[] b) {
            if (a[0] < b[0]) {
                return -1;
            } else if (a[0] > b[0]) {
                return 1;
            }
            if (a[1] < b[1]) {
                return -1;
            } else if (a[1] > b[1]) {
                return 1;
            }
            return 0;
        }
    }

    private static BTree<long[]> set = new BTree<>(8, new TupleComparator());
    public static void setup() {
        for (int i = 0; i < 100 * 6; ++i) {
            for (int k = 0; k < 20; ++k) {
                long[] pair = new long[] {i, k};
                set.add(pair);
            }
        }
    }

    public static void run() {
        Thread t1 = new Thread() {
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 0; i < 100; ++i)
                    for (int k = 0; k < 20; ++k)
                        set.contains(new long[]{i, k}, h);
            }
        };
        Thread t2 = new Thread() {
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 100; i < 2 * 100; ++i)
                    for (int k = 0; k < 20; ++k)
                        set.contains(new long[]{i, k}, h);
            }
        };
        Thread t3 = new Thread() {
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 2 * 100; i < 3 * 100; ++i)
                    for (int k = 0; k < 20; ++k)
                        set.contains(new long[]{i, k}, h);
            }
        };
        Thread t4 = new Thread() {
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 3 * 100; i < 4 * 100; ++i)
                    for (int k = 0; k < 20; ++k)
                        set.contains(new long[]{i, k}, h);
            }
        };
        Thread t5 = new Thread() {
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 4 * 100; i < 5 * 10; ++i)
                    for (int k = 0; k < 20; ++k)
                        set.contains(new long[]{i, k}, h);
            }
        };

        Thread t6 = new Thread() {
            public void run() {
                BTree.Hints h = set.newHints();
                for (int i = 5 * 100; i < 6 * 100; ++i)
                    for (int k = 0; k < 20; ++k)
                        set.contains(new long[]{i, k}, h);
            }
        };
        t1.start(); 
        t2.start(); 
        t3.start(); 
        t4.start(); 
        t5.start(); 
        t6.start(); 


        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
