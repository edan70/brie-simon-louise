package benchmarks;
import BTree.BTree;
import java.util.Comparator; 

public class SSSequentialSearchOrder16 {
	private static class TupleComparator implements Comparator<long[]>{
		public boolean equal(long[] a, long[] b) {
			return a[0] == b[0] && a[1] == b[1];
		}
		@Override
		public int compare(long[] a, long[] b) {
			if (a[0] < b[0]) {
				return -1;
			} else if (a[0] > b[0]) {
				return 1;
			}
			if (a[1] < b[1]) {
				return -1;
			} else if (a[1] > b[1]) {
				return 1;
			}
			return 0;
		}
	}
    private static BTree<long[]> bt = new BTree<long[]>(16, new TupleComparator());
    public static void setup() {
        BTree.Hints h = bt.newHints();
        for (long i = 0; i < 10000; ++i)
            for (long k = 0; k < 2000; ++k)
                bt.add(new long[] {i,k}, h);
    }

    public static void run() {
       BTree.Hints h = bt.newHints();
       for (long i = 0; i < 10000; ++i)
            for (long k = 0; k < 2000; ++k)
                bt.contains(new long[] {i,k}, h); 
    }
}
