package benchmarks;
import java.util.Comparator;
import java.util.*;
import java.util.concurrent.*;
public class RandomParInsertSkipList {
    private static class TupleComparator implements Comparator<long[]>{
		public boolean equal(long[] a, long[] b) {
			return a[0] == b[0] && a[1] == b[1];
		}

		@Override
		public int compare(long[] a, long[] b) {
			if (a[0] < b[0]) {
				return -1;
			} else if (a[0] > b[0]) {
				return 1;
			}
			if (a[1] < b[1]) {
				return -1;
			} else if (a[1] > b[1]) {
				return 1;
			}
			return 0;
		}
	}

    public static void run() {
        Random r = new Random(666);
        ConcurrentSkipListSet<long[]> set = new ConcurrentSkipListSet<>(new TupleComparator());
        Thread t1 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 100; ++i)
                    for (int j = 0; j < 10; ++j)
                        set.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t2 = new Thread() {
            @Override
            public void run() {
                for (int i = 100; i < 200; ++i)
                    for (int j = 0; j < 10; ++j)
                        set.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        Thread t3 = new Thread() {
            @Override
            public void run() {
                for (int i = 200; i < 300; ++i)
                    for (int j = 0; j < 10; ++j)
                        set.add(new long[] {r.nextLong() , r.nextLong()});
            }
        };

        Thread t4 = new Thread() {
            @Override
            public void run() {
                for (int i = 300; i < 400; ++i)
                    for (int j = 0; j < 10; ++j)
                        set.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };

        Thread t5 = new Thread() {
            @Override
            public void run() {
                for (int i = 400; i < 500; ++i)
                    for (int j = 0; j < 10; ++j)
                        set.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };

        Thread t6 = new Thread() {
            @Override
            public void run() {
                for (int i = 500; i < 600; ++i)
                    for (int j = 0; j < 10; ++j)
                        set.add(new long[] {r.nextLong(), r.nextLong()});
            }
        };
        
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
       
        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
