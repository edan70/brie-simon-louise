package benchmarks;
import BTree.BTree;
import java.util.Comparator;
import java.util.Random;
import java.util.*;
import java.util.concurrent.*;

public class MemUsageTreeSet10K {
    private static final int TEN_SECONDS = 10 * 1000;
    private static final int TWO_MIN     = 120 * 1000;

    private static class TupleComparator implements Comparator<long[]>{
        public boolean equal(long[] a, long[] b) {
            return a[0] == b[0] && a[1] == b[1];

        }
        @Override
        public int compare(long[] a, long[] b) {
            if (a[0] < b[0]) {
                return -1;
            } else if (a[0] > b[0]) {
                return 1;
            }
            if (a[1] < b[1]) {
                return -1;
            } else if (a[1] > b[1]) {
                return 1;
            }
            return 0;
        }
    }

    public static void run() {
        long memBefore, memAfter, memDiff;
        System.out.println("Starting! Sleeping for 10s first...");
        memBefore = Runtime.getRuntime().freeMemory();
        try {
            Thread.sleep(TEN_SECONDS);
        } catch (Exception e){
            e.printStackTrace();
        }
        int numInserts = 10000;
        SortedSet<long[]> set = Collections.synchronizedSortedSet(new TreeSet<long[]>(new TupleComparator()));
        for (int i = 0; i < numInserts; ++i)
            set.add(new long[] {i, i});

        memAfter = Runtime.getRuntime().freeMemory();
        memDiff = memBefore - memAfter;
        System.out.println("Memusage at 10K insertions: " + memDiff);
        System.out.println("Inserted all elements, now sleeping for 10s");
        try {
            Thread.sleep(TEN_SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
