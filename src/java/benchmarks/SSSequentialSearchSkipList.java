package benchmarks;
import java.util.*; 
import java.util.concurrent.*;
public class SSSequentialSearchSkipList {
	private static class TupleComparator implements Comparator<long[]>{
		public boolean equal(long[] a, long[] b) {
			return a[0] == b[0] && a[1] == b[1];
		}
		@Override
		public int compare(long[] a, long[] b) {
			if (a[0] < b[0]) {
				return -1;
			} else if (a[0] > b[0]) {
				return 1;
			}
			if (a[1] < b[1]) {
				return -1;
			} else if (a[1] > b[1]) {
				return 1;
			}
			return 0;
		}
	}
    private static ConcurrentSkipListSet<long[]> bt = new ConcurrentSkipListSet<long[]>(new TupleComparator());

    public static void setup() {
        for (long i = 0; i < 10000; ++i)
            for (long k = 0; k < 2000; ++k)
                bt.add(new long[] {i,k});
    }

    public static void run() {
       for (long i = 0; i < 10000; ++i)
            for (long k = 0; k < 2000; ++k)
                bt.contains(new long[] {i,k}); 
    }
}
