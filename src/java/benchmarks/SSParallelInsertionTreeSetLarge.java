package benchmarks;
import java.util.Comparator;
import java.util.*;
import java.util.concurrent.*;
public class SSParallelInsertionTreeSetLarge {
    private static class TupleComparator implements Comparator<long[]>{
        public boolean equal(long[] a, long[] b) {
            return compare(a, b) == 0; 
        }
        @Override
        public int compare(long[] a, long[] b) {
            // Assume equal length
            for (int i = 0; i < a.length; ++i) {
                if (a[i] < b[i])
                    return -1;
                if (a[i] > b[i])
                    return 1;
            }

            return 0;
        }
    }

    public static void run() {
        SortedSet<long[]> set = Collections.synchronizedSortedSet(new TreeSet<long[]>(new TupleComparator())); 
        Thread t1 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 4 * 2; ++i)
                    for (int ii = 0; ii < 4 * 2; ++ii)
                        for (int iii = 0; iii < 4; ++iii)
                            for (int iv = 0; iv < 4; ++iv)
                                for (int v = 0; v < 4; ++v)
                                    for (int vi = 0; vi < 4; ++vi)
                                        for (int vii = 0; vii < 4; ++vii)
                                            for (int viii = 0; viii < 4; ++viii)
                                                for (int ix = 0; ix < 4; ++ix)
                                                    for (int x = 0; x < 4 ; ++x)
                                                        set.add(new long[] {i, ii, iii, iv, v, vi, vii, viii, ix, x});
            }
        };
        Thread t2 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 4 * 2; ++i)
                    for (int ii = 0; ii < 4 * 2; ++ii)
                        for (int iii = 0; iii < 4; ++iii)
                            for (int iv = 0; iv < 4; ++iv)
                                for (int v = 0; v < 4; ++v)
                                    for (int vi = 0; vi < 4; ++vi)
                                        for (int vii = 0; vii < 4; ++vii)
                                            for (int viii = 0; viii < 4; ++viii)
                                                for (int ix = 0; ix < 4; ++ix)
                                                    for (int x = 0; x < 4 ; ++x)
                                                        set.add(new long[] {i, ii, iii, iv, v, vi, vii, viii, ix, x});
            }
        };
        Thread t3 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 4 * 2; ++i)
                    for (int ii = 0; ii < 4 * 2; ++ii)
                        for (int iii = 0; iii < 4; ++iii)
                            for (int iv = 0; iv < 4; ++iv)
                                for (int v = 0; v < 4; ++v)
                                    for (int vi = 0; vi < 4; ++vi)
                                        for (int vii = 0; vii < 4; ++vii)
                                            for (int viii = 0; viii < 4; ++viii)
                                                for (int ix = 0; ix < 4; ++ix)
                                                    for (int x = 0; x < 4 ; ++x)
                                                        set.add(new long[] {i, ii, iii, iv, v, vi, vii, viii, ix, x});
            }
        };
        Thread t4 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 4 * 2; ++i)
                    for (int ii = 0; ii < 4 * 2; ++ii)
                        for (int iii = 0; iii < 4; ++iii)
                            for (int iv = 0; iv < 4; ++iv)
                                for (int v = 0; v < 4; ++v)
                                    for (int vi = 0; vi < 4; ++vi)
                                        for (int vii = 0; vii < 4; ++vii)
                                            for (int viii = 0; viii < 4; ++viii)
                                                for (int ix = 0; ix < 4; ++ix)
                                                    for (int x = 0; x < 4 ; ++x)
                                                        set.add(new long[] {i, ii, iii, iv, v, vi, vii, viii, ix, x});
            }
        };
        Thread t5 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 4 * 2; ++i)
                    for (int ii = 0; ii < 4 * 2; ++ii)
                        for (int iii = 0; iii < 4; ++iii)
                            for (int iv = 0; iv < 4; ++iv)
                                for (int v = 0; v < 4; ++v)
                                    for (int vi = 0; vi < 4; ++vi)
                                        for (int vii = 0; vii < 4; ++vii)
                                            for (int viii = 0; viii < 4; ++viii)
                                                for (int ix = 0; ix < 4; ++ix)
                                                    for (int x = 0; x < 4 ; ++x)
                                                        set.add(new long[] {i, ii, iii, iv, v, vi, vii, viii, ix, x});
            }
        };
        Thread t6 = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 4 * 2; ++i)
                    for (int ii = 0; ii < 4 * 2; ++ii)
                        for (int iii = 0; iii < 4; ++iii)
                            for (int iv = 0; iv < 4; ++iv)
                                for (int v = 0; v < 4; ++v)
                                    for (int vi = 0; vi < 4; ++vi)
                                        for (int vii = 0; vii < 4; ++vii)
                                            for (int viii = 0; viii < 4; ++viii)
                                                for (int ix = 0; ix < 4; ++ix)
                                                    for (int x = 0; x < 4 ; ++x)
                                                        set.add(new long[] {i, ii, iii, iv, v, vi, vii, viii, ix, x});
            }
        };
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
            t6.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
