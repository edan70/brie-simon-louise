#include "invoke.h"
#include <stdlib.h>
#include <unistd.h>
#define BILLION 1000000000
#define CLASSPATH "-Djava.class.path=/mnt/c/Users/Simon Tenggren/git/brie-simon-louise/build/classes/java/main"
JNIEnv* create_vm() {
    JavaVM* jvm;
    JNIEnv* env;
    JavaVMInitArgs args;
    JavaVMOption options[3];
    /* DISABLE JIT */
    /* set use class and native library path */
    options[0].optionString  = CLASSPATH;
    options[1].optionString  = "-Djava.library=.";
    /* verbose output */
    options[2].optionString  = "-verbose:jni";
    /* Print JNI-relate messages */
    args.version = JNI_VERSION_1_8;
    args.ignoreUnrecognized = 1;
    args.options = options;
    args.nOptions = 3;

    int res = JNI_CreateJavaVM(&jvm, (void **)&env, &args);
    if (res != JNI_OK){
        fprintf(stderr,"Something went wrong and the environment couldnt be started!\n");
        exit(1);
    } else 
        printf("VM Successfully created!\n");
    return env;
}
JNIEnv* create_vm_ss(JavaVM* jvm) {
    JNIEnv* env;
    JavaVMInitArgs args;
    JavaVMOption options[3];
    /* DISABLE JIT */
    /* set use class and native library path */
    options[0].optionString  = CLASSPATH;
    options[1].optionString  = "-Djava.library=.";
    /* verbose output */
    options[2].optionString  = "-verbose:jni";
    /* Print JNI-relate messages */
    args.version = JNI_VERSION_1_8;
    args.ignoreUnrecognized = 1;
    args.options = options;
    args.nOptions = 3;

    int res = JNI_CreateJavaVM(&jvm, (void **)&env, &args);
    if (res != JNI_OK){
        fprintf(stderr,"Something went wrong and the environment couldnt be started!\n");
        exit(1);
    } else 
        printf("VM Successfully created!\n");
    return env;
}

void invoke_class(JNIEnv* env, char* path, char* methodName, char* methodSignature){
    jclass helloWorldClass;
    jmethodID mainMethod;
    jobjectArray applicationArgs;
    jstring applicationArg0;
    helloWorldClass = (*env)->FindClass(env, path);
    if (helloWorldClass == NULL) {
        fprintf(stderr,"Class couldn't be found!\n");
        if ((*env)->ExceptionOccurred(env))
            (*env)->ExceptionDescribe(env);
        exit(1);
    } 
    mainMethod = (*env)->GetStaticMethodID(env, helloWorldClass, methodName, methodSignature);
    if (mainMethod == 0) {
        fprintf(stderr, "Method not found!\n");
        exit(1);
    }
    (*env)->CallStaticVoidMethod(env, helloWorldClass, mainMethod, NULL);
}

jclass invoke_and_get_class(JNIEnv* env, char* path){
    jclass class;
    class = (*env)->FindClass(env, path);
    if (class == NULL) {
        fprintf(stderr,"Class couldn't be found!\n");
        if ((*env)->ExceptionOccurred(env))
            (*env)->ExceptionDescribe(env);
        exit(1);
    } 
    return class;
}

void run_setup(JNIEnv* env, jclass class) {
    jmethodID setup;
    setup = (*env)->GetStaticMethodID(env, class, SETUP_NAME, VOID_SIG);
    if (setup == 0) {
        fprintf(stderr, "Method not found!\n");
        if ((*env)->ExceptionOccurred(env))
            (*env)->ExceptionDescribe(env);
        exit(1);
    }
    (*env)->CallStaticVoidMethod(env, class, setup, NULL);
}

void run_benchmark(JNIEnv* env, jclass class) {
    jmethodID run;
    run = (*env)->GetStaticMethodID(env, class, RUN_NAME, VOID_SIG);
    if (run == 0) {
        fprintf(stderr, "Method not found!\n");
        if ((*env)->ExceptionOccurred(env))
            (*env)->ExceptionDescribe(env);

        exit(1);
    }
    (*env)->CallStaticVoidMethod(env, class, run, NULL);
}

void garbage_collect(JNIEnv* env) 
{
    jclass systemClass = NULL;
    jmethodID systemGCMethod = NULL;
    
    systemClass = (*env)->FindClass(env, "java/lang/System");
    systemGCMethod = (*env)->GetStaticMethodID(env, systemClass, "gc", VOID_SIG);
    (*env)->CallStaticVoidMethod(env, systemClass, systemGCMethod);
}
