#ifndef INVOKE_H
#define INVOKE_H
#include <stdio.h>
#include <jni.h>
#define VOID_SIG "()V"
#define MAIN_SIG "([Ljava/lang/String;)V"
#define RUN_NAME "run"
#define SETUP_NAME "setup"
#include <time.h>
#define BILLION 1000000000
#define VM_INVOC 50
#define BM_RUNS 10
/**
 * @fn timespec_diff(struct timespec *, struct timespec *, struct timespec *)
 * @brief Compute the diff of two timespecs, that is a - b = result.
 * @param a the minuend
 * @param b the subtrahend
 * @param result a - b
 */
static inline void timespec_diff(struct timespec *a, struct timespec *b,
    struct timespec *result) {
    result->tv_sec  = a->tv_sec  - b->tv_sec;
    result->tv_nsec = a->tv_nsec - b->tv_nsec;
    if (result->tv_nsec < 0) {
        --result->tv_sec;
        result->tv_nsec += 1000000000L;
    }
}
JNIEnv* create_vm();
JNIEnv* create_vm_ss(JavaVM*);
void invoke_class(JNIEnv* env, char* path, char* methodName, char* methodSignature);
jclass invoke_and_get_class(JNIEnv* env, char* path);
void run_setup(JNIEnv* env, jclass c);
void run_benchmark(JNIEnv* env, jclass c);
void garbage_collect(JNIEnv* env);
#endif
