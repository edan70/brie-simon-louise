#include "invoke.h"
#include <time.h>
#include <inttypes.h>

int main(void)
{
    struct timespec start_cl, end_cl, start_bm, end_bm, diff_cl, diff_bm;
    long long total_cl, total_bm, sum;
    JNIEnv* env = create_vm();

    FILE* appendTo = fopen("out_ss/startup_random_par_search_32.txt", "a");
    timespec_get(&start_cl, TIME_UTC);
    jclass class = invoke_and_get_class(env, "benchmarks/RandomParSearchOrder32");
    timespec_get(&end_cl, TIME_UTC);

    timespec_diff(&end_cl, &start_cl, &diff_cl);
    total_cl = diff_cl.tv_sec * BILLION + diff_cl.tv_nsec;

    run_setup(env, class);

    timespec_get(&start_bm, TIME_UTC);
    run_benchmark(env, class);
    timespec_get(&end_bm, TIME_UTC);

    timespec_diff(&end_bm, &start_bm, &diff_bm);
    total_bm = diff_bm.tv_sec * BILLION + diff_bm.tv_nsec;

    sum = total_bm + total_cl; 

    fprintf(appendTo, "CL = %lld BENCHMARK = %lld TOTAL = %lld \n", total_cl, total_bm, sum);
}
