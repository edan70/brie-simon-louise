#include "invoke.h"
#include <time.h>
#include <inttypes.h>
int main(void)
{
    struct timespec start_cl, end_cl, start_bm, end_bm, diff_cl, diff_bm;
    long long total_cl, total_bm, bm_nsec, bm_sec, cl_nsec, cl_sec, sum;
    JNIEnv* env = create_vm();
    FILE* appendTo = fopen("out_ss/startup_random_par.txt", "a");
    timespec_get(&start_cl, TIME_UTC);
    jclass class = invoke_and_get_class(env, "benchmarks/RandomParInsert"); /* How do we get the class loading time? Simply add?*/
    timespec_get(&end_cl, TIME_UTC);

    timespec_diff(&end_cl, &start_cl, &diff_cl);
    total_cl = diff_cl.tv_sec * BILLION + diff_cl.tv_nsec;

    timespec_get(&start_bm, TIME_UTC);
    run_benchmark(env, class);
    timespec_get(&end_bm, TIME_UTC);

    timespec_diff(&end_bm, &start_bm, &diff_bm);
    total_bm = diff_bm.tv_sec * BILLION + diff_bm.tv_nsec;

    sum = total_bm + total_cl; 

    fprintf(appendTo, "CL = %lld BENCHMARK = %lld total = %lld \n", total_cl, total_bm, sum);
}
