#include "invoke.h"
#include <time.h>
#include <inttypes.h>
int main(void)
{
    struct timespec start_bm, end_bm, diff_bm;
    FILE* appendTo = fopen("out_steady/par_search16.txt", "a");
    JNIEnv* env = create_vm();
    
    jclass class = invoke_and_get_class(env, "benchmarks/SSParallelSearch16"); 
    run_setup(env, class);
    fprintf(appendTo, "New invocation\n\n");
    for (int i = 0; i < BM_RUNS; ++i) {
            printf("New run\n");
            timespec_get(&start_bm, TIME_UTC);
            run_benchmark(env, class);
            timespec_get(&end_bm, TIME_UTC);
            timespec_diff(&end_bm, &start_bm, &diff_bm);
            fprintf(appendTo, "Seconds = %ld Nano = %ld \n", diff_bm.tv_sec, diff_bm.tv_nsec);
    }
}
