#include "invoke.h"
#include <time.h>
#include <inttypes.h>

int main(void)
{
    JNIEnv* env = create_vm();
    jclass class = invoke_and_get_class(env, "benchmarks/MemUsageBTree10KOrder8");
    run_benchmark(env, class);
}
