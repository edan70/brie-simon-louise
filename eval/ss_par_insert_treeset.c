#include "invoke.h"
#include <time.h>
#include <inttypes.h>

int main(void)
{
    struct timespec start_bm, end_bm, diff_bm;
    long long int time;
    int i, k;
    JNIEnv* env;
    JavaVM* vm;
    jclass class;
    FILE* appendTo = fopen("out_steady/par_insert_treeset.txt", "a");

    fprintf(appendTo, "New invocation\n\n");
    env = create_vm_ss(vm);    
    class = invoke_and_get_class(env, "benchmarks/SSParallelInsertionTreeSet"); 
    for (k = 0; k < BM_RUNS; ++k) {
        printf("New run\n");
        timespec_get(&start_bm, TIME_UTC);
        run_benchmark(env, class); 
        timespec_get(&end_bm, TIME_UTC);
        timespec_diff(&end_bm, &start_bm, &diff_bm);
        fprintf(appendTo, "Seconds = %ld Nano = %ld \n", diff_bm.tv_sec, diff_bm.tv_nsec);
        garbage_collect(env);
    }
}

