#include "invoke.h"
#include <time.h>
#include <inttypes.h>
int main(void)
{
    struct timespec start_cl, end_cl, start_r, end_r, diff_cl, diff_bm;
    long long int cl_time, r_time, total;

    JNIEnv* env = create_vm();
    FILE* appendTo = fopen("out_ss/startup_seq_insert_8.txt", "a");

    /* Load class */
    timespec_get(&start_cl, TIME_UTC);
    jclass class = invoke_and_get_class(env, "benchmarks/SequentialInsertion8");
    timespec_get(&end_cl, TIME_UTC);
    
    timespec_diff(&end_cl, &start_cl, &diff_cl);

    cl_time = diff_cl.tv_sec * BILLION + diff_cl.tv_nsec;
    /* Run benchmark */
    timespec_get(&start_r, TIME_UTC);
    run_benchmark(env, class);
    timespec_get(&end_r, TIME_UTC);

    timespec_diff(&end_r, &start_r, &diff_bm);
    r_time = diff_bm.tv_sec * BILLION + diff_bm.tv_nsec;

    total = cl_time + r_time; 
    fprintf(appendTo, "CL = %lld BENCHMARK = %lld TOTAL = %lld \n", cl_time, r_time, total);
}
